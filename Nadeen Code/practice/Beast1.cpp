//
//  Beast1.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Beast1.hpp"

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>


sf::Clock c;

Beast1:: Beast1()
{
    beast.setSize(sf::Vector2f(168, 160));
    beasttexture.loadFromFile(resourcePath() + "beast1sprite.png");
    beast.setTexture(&beasttexture);
    sf::Vector2u texturesize = beasttexture.getSize();
    texturesize.x /= 2;
    texturesize.y /= 1;
}

void Beast1::setexplosion ()
{
    beasttexture.loadFromFile(resourcePath() + "explosion.png");
    beast.setTexture(&beasttexture);
}

void Beast1::settexturerect(int n)
{
    beast.setTextureRect(sf::IntRect(texturesize.x * n, texturesize.y * 0, texturesize.x, texturesize.y));
    
}

void Beast1::setpos(float x, float y) {
    beast.setPosition(x,y);
}

void Beast1::draw(sf::RenderWindow&window)
{
    window.draw(beast);
}

int Beast1::getX()
{
    return beast.getPosition().x;
}

int Beast1::getY() {
    return beast.getPosition().y;
    
}

void Beast1::movebeast(int speed)
{
    beast.move(0, speed);
}


bool Beast1::checkcoll(bullet bullet)
{
    if ((bullet.getRight() > beast.getPosition().x) && (bullet.getTop() < beast.getPosition().y + beast.getSize().y)
        && (bullet.getBottom() > beast.getPosition().y) )
        return true;
    else return false;
}




