//
//  Chicken.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Chicken_hpp
#define Chicken_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
#include <stdlib.h>
#include "ResourcePath.hpp"
#include "bullet.hpp"


class Chicken
{
private:
    
    sf::Texture chickentexture;
    sf::Vector2u texturesize;
    
    
public:
    sf::RectangleShape chicken;
    bool dead=false;
    Chicken();
    void settexturerect(int n);
    void setrect(sf::IntRect s);
    void setpos(int x, int y);
    void checkcoll(bullet bullet);
    void draw (sf::RenderWindow&window);
    sf::Vector2u gettexturesize();
    int getX();
    int getY();
    bool checkDead();
    
};


#endif /* Chicken_hpp */
