//
//  weapons.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "weapons.hpp"


weapons::weapons() {
    
    srand(unsigned(time(NULL)));
    
    int x=rand()%3; //0 for fork, 1 for blast, 2 for beam
    
    if (!p1.loadFromFile(resourcePath() + "pickup1.png")) {
        return EXIT_FAILURE; }
    
    if (!p2.loadFromFile(resourcePath() + "pickup2.png")) {
        return EXIT_FAILURE; }

    if (!p3.loadFromFile(resourcePath() + "pickup3.png")) {
        return EXIT_FAILURE; }

    wep.setSize(sf::Vector2f(200,200));
    
    switch(x) {
        case 0: {
            type=0;
            wep.setTexture(&p1);
        }
        case 1: {
            type=1;
            wep.setTexture(&p2);
        }
        case 2: {
            type=2;
            wep.setTexture(&p3);
        }
    }
}

void weapons::move() {
    
    wep.move(0,30);
    
}


void weapons::draw(sf::RenderWindow& window) {
    window.draw(wep);
    
    
}


void weapons::setPos(int x, int y) {
 wep.setPosition(x, y);
            

}
