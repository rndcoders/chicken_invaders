//
//  eggs.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "eggs.hpp"

eggs::eggs()
{
    egg.setSize(sf::Vector2f(47, 34));
    
    if (!eggtexture.loadFromFile(resourcePath() + "eggsprite.png")) {
        return EXIT_FAILURE;
    }
    
    egg.setTexture(&eggtexture);
    egg.setScale(2, 2);
    
    sf::IntRect size(0, 0, 47, 34);
    
    
}

void eggs::setrect(sf::IntRect size) {
    egg.setTextureRect(size);
}

void eggs::setpos(int x, int y) {
    egg.setPosition(x,y);
}


void eggs::draw(sf::RenderWindow&window)
{
    window.draw(egg);
}

void eggs::moveegg(int speed) {
    egg.move(0, speed);
}

int eggs::getX() {
    return egg.getPosition().x;
    
}

int eggs::getY() {
    return egg.getPosition().y;
    
}

