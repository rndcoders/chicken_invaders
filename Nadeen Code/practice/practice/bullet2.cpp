//
//  bullet2.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/24/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "bullet2.hpp"
#include "player2.hpp"
#include "ResourcePath.hpp"
#include <iostream>
using namespace std;



bullet2::bullet2() {
    
    if (!texture.loadFromFile(resourcePath() + "bullet2.png")) {
        return EXIT_FAILURE; }
    
    sf::IntRect size(0,0,128,128);
    bull.setSize(sf::Vector2f (128,128));
    bull.setTexture(&texture);
    bull.setScale(0.8, 0.8);
    bull.setFillColor(sf::Color::Red);
    
    
    
}

void bullet2::fire(){
    bull.move(0, -5);
}

void bullet2::draw(sf::RenderWindow& window) {
    window.draw(bull);
    
}

void bullet2::setpos(int x,int y) {
    bull.setPosition(x, y);
}

void bullet2::handle_events(sf::Event event, bool& fire) {
    switch(event.key.code) {
        case sf::Keyboard::LShift:
            fire=true;
            break;
    }
}

int bullet2::getLeft() {
    return bull.getPosition().x;
    
}

int bullet2::getTop() {
    return bull.getPosition().y;
    
}

int bullet2::getRight() {
    return bull.getPosition().x + bull.getSize().x;
}

int bullet2::getBottom() {
    return bull.getPosition().y + bull.getSize().y;
    
}


bool bullet2::isFire(bool& fire){
    return fire;
}
