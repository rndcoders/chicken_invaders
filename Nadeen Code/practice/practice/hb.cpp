//
//  hb.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "hb.hpp"

hb::hb() {
 
    bar.setSize(sf::Vector2f(204, 30));
    sf::Texture bartexture;
    
    if (!bartexture.loadFromFile(resourcePath() + "barsprite.png")) {
        return EXIT_FAILURE;
    }
    
    bar.setTexture(&bartexture);
    
    sf::IntRect size(0, 0, 204, 30);
    
    bar.setTexture(&bartexture);
    
}

void hb::setrect(sf::IntRect size) {
    bar.setTextureRect(size);
    
}


void hb::setpos(int x, int y) {
    bar.setPosition(x,y);
}

void hb::draw(sf::RenderWindow&window)
{
    window.draw(bar);
}

