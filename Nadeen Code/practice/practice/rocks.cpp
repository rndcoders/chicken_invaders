//
//  rocks.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/24/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "rocks.hpp"
#include <time.h>
#include <stdlib.h>
#include <iostream>

using namespace std;


rocks::rocks(){
    
    sf::IntRect s(0,0,319,372);
    
    r1.setSize(sf::Vector2f (319,327));
    r2.setSize(sf::Vector2f (319,327));
    r3.setSize(sf::Vector2f (319,327));
    
    if (!rock1.loadFromFile(resourcePath() + "rock1sprite.png")) {
        return EXIT_FAILURE; }
    r1.setTexture(&rock1);
    
    if (!rock2.loadFromFile(resourcePath() + "rock2sprite.png")) {
        return EXIT_FAILURE; }
    r2.setTexture(&rock2);
    
    if (!rock3.loadFromFile(resourcePath() + "rock3sprite.png")) {
        return EXIT_FAILURE; }
    r3.setTexture(&rock3);

    
    
r1.setScale(1,1);
r2.setScale(1,1);
r3.setScale(1,1);
    
    
r1.setTextureRect(s);
r2.setTextureRect(s);
r3.setTextureRect(s);

    
    srand(unsigned(time(NULL)));
    x= rand()%3;
    
}

void rocks::draw(sf::RenderWindow& window) {
    
    if(shot==false) {
    switch(x) {
       case 0: window.draw(r1);
       case 1: window.draw(r2);
       case 2: window.draw(r3);
}
    
    }

}

void rocks::setrect(sf::IntRect size) {
    r1.setTextureRect(size);
    r2.setTextureRect(size);
    r3.setTextureRect(size);
}

void rocks::move(int x, int y) {
    r1.move(x,y);
    r2.move(x,y);
    r3.move(x,y);
    
}


void rocks::setPos(int x, int y) {
    switch(x) {
        case 0: r1.setPosition(sf::Vector2f(x,y));
        case 1: r2.setPosition(sf::Vector2f(x,y));
        case 2: r3.setPosition(sf::Vector2f(x,y));
    }
    /*
    srand(unsigned(time(NULL)));
    int x=rand()%1001;
    int y=rand()%1001;
    
    switch(x) {
        case 0: r1.setPosition(sf::Vector2f(x,y));
        case 1: r2.setPosition(sf::Vector2f(x,y));
        case 2: r3.setPosition(sf::Vector2f(x,y));
    }
     */
}
int rocks::getx() {
    switch(x) {
        case 0: return r1.getPosition().x;
        case 1: return r2.getPosition().x;
        case 2: return r3.getPosition().x;
    }
    
    
}

int rocks::gety() {
    switch(x) {
        case 0: return r1.getPosition().y;
        case 1: return r2.getPosition().y;
        case 2: return r3.getPosition().y;
    }

    
}

int rocks::getLeft() {
    switch(x) {
        case 0: return r1.getPosition().x;
        case 1: return r2.getPosition().x;
        case 2: return r3.getPosition().x;
    }
    
}

int rocks::getTop() {
    switch(x) {
        case 0: return r1.getPosition().y;
        case 1: return r2.getPosition().y;
        case 2: return r3.getPosition().y;
    }

    
}

int rocks::getRight() {
    switch(x) {
        case 0: return r1.getPosition().x +319;
        case 1: return r2.getPosition().x +319;
        case 2: return r3.getPosition().x +319;
    }
}

int rocks::getBottom() {
    switch(x) {
        case 0: return r1.getPosition().y+319;
        case 1: return r2.getPosition().y+319;
        case 2: return r3.getPosition().y+319;
    }


    
}

void rocks::checkCollision(bullet bull) {
    
 
    
    
 
    switch(x) {
        case 0: {
            
            if(bull.getLeft()>r1.getPosition().x && bull.getRight() <r1.getPosition().x +r1.getSize().x && bull.getBottom() < r1.getPosition().y+ r1.getSize().y && r1.getPosition().y < bull.getTop()) {
                //cout<< "collision"<<endl;
                shot=true;
                }
        }
        case 1: {
            
            if(bull.getLeft()>r2.getPosition().x && bull.getRight() <r2.getPosition().x +r2.getSize().x && bull.getBottom() < r2.getPosition().y+ r2.getSize().y && r1.getPosition().y < bull.getTop()) {
                //cout<< "collision"<<endl;
                shot=true;
            }
        }
        case 2: {
            
            if(bull.getLeft()>r3.getPosition().x && bull.getRight() <r3.getPosition().x +r3.getSize().x && bull.getBottom() < r3.getPosition().y+ r3.getSize().y && r3.getPosition().y < bull.getTop()) {
                //cout<< "collision"<<endl;
                shot=true;
                
            }
        }
            
    }
    
    
}


void rocks::checkCollision(bullet2 bull) {
    
    switch(x) {
        case 0: {
            
            if(bull.getLeft()>r1.getPosition().x && bull.getRight() <r1.getPosition().x +r1.getSize().x && bull.getBottom() < r1.getPosition().y+ r1.getSize().y && r1.getPosition().y < bull.getTop()) {
                //cout<< "collision"<<endl;
                shot=true;
            }
        }
        case 1: {
            
            if(bull.getLeft()>r2.getPosition().x && bull.getRight() <r2.getPosition().x +r2.getSize().x && bull.getBottom() < r2.getPosition().y+ r2.getSize().y && r1.getPosition().y < bull.getTop()) {
                //cout<< "collision"<<endl;
                shot=true;
                
            }
        }
        case 2: {
            
            if(bull.getLeft()>r3.getPosition().x && bull.getRight() <r3.getPosition().x +r3.getSize().x && bull.getBottom() < r3.getPosition().y+ r3.getSize().y && r3.getPosition().y < bull.getTop()) {
                //cout<< "collision"<<endl;
                shot=true;
                
            }
        }
            
    }
    
    
}

bool rocks::isShot() {
    return(shot);
}



