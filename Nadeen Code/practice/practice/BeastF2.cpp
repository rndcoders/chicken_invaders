//
//  BeastF2.cpp
//  practice
//
//  Created by Nadeen Mohamed on 5/1/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "BeastF2.hpp"


BeastF2::BeastF2()
{
    beast.setSize(sf::Vector2f(249, 267));
   
    
    if (!beasttexture.loadFromFile(resourcePath() +"FB2sprite.png")) {
        return ;
    }
    
    beast.setTexture(&beasttexture);
    
    sf::IntRect size(0, 0, 249, 267);
    beast.setTextureRect(size);  
    
    //sf::Vector2u texturesize = beasttexture.getSize();
    //texturesize.x /= 2;
    //texturesize.y /= 1;
}

void BeastF2::setexplosion()
{
    beasttexture.loadFromFile(resourcePath() +"explosionFB2.png");
    beast.setTexture(&beasttexture);
}

void BeastF2::setrect(sf::IntRect size) {
    beast.setTextureRect(size);
}

//void Beast1::settexturerect(int n)
//{
//beast.setTextureRect(sf::IntRect(texturesize.x * n, texturesize.y * 0, texturesize.x, texturesize.y));

//}

void BeastF2::setpos(int x, int y) {
    beast.setPosition(x, y);
}

void BeastF2::draw(sf::RenderWindow&window)
{
    window.draw(beast);
}

int BeastF2::getX()
{
    return beast.getPosition().x;
}

int BeastF2::getY() {
    return beast.getPosition().y;
    
}

void BeastF2::movebeast(int x, int y)
{
    beast.move(x, y);
}


bool BeastF2::checkcoll(bullet bullet)
{
if ((bullet.getRight() > beast.getPosition().x) && (bullet.getTop() < beast.getPosition().y + beast.getSize().y)
&& (bullet.getBottom() > beast.getPosition().y) )
return true;
else return false;
}




