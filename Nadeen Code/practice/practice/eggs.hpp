//
//  eggs.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef eggs_hpp
#define eggs_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
#include <stdlib.h>
#include "ResourcePath.hpp"

class eggs{
private:
    
    sf::Texture eggtexture;
    sf::Vector2u texturesize;
    
public:
    eggs();
    sf::RectangleShape egg;
    void setrect(sf::IntRect size);
    void setpos(int x, int y);
    void draw(sf::RenderWindow&window);
    void moveegg(int speed);
    int getX();
    int getY();

};

#endif /* eggs_hpp */
