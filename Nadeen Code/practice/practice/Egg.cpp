//
//  Egg.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Egg.hpp"
#include <SFML/Graphics.hpp>

#include <string>

#include <iostream>



sf::Clock c;

Egg::Egg ()

{
    
    egg.setSize(sf::Vector2f(47, 34));
    
    sf::Texture eggtexture;
    
    
    if (!eggtexture.loadFromFile(resourcePath() + "eggsprite.png")) {
        return EXIT_FAILURE; }
    
  
    
    egg.setTexture(&eggtexture);
    
    sf::Vector2u texturesize = eggtexture.getSize();
    
    texturesize.x /= 5;
    
    texturesize.y /= 1;
    
}

void Egg::settexturerect(int n)

{
    
    egg.setTextureRect(sf::IntRect(texturesize.x * n, texturesize.y * 0, texturesize.x,
                                   
                                   texturesize.y));
    
}

void Egg::setpos(sf::Vector2f newPos) {
    
    egg.setPosition(newPos);
    
}

void Egg::draw(sf::RenderWindow&window)

{
    
    window.draw(egg);
    
}

void Egg::moveegg(int speed)

{
    
    egg.move(0, speed);
    
}

int Egg::getX() {
    
    return egg.getPosition().x;
    
}

int Egg::getY() {
    
    return egg.getPosition().y;
    
}
