//
//  Beast1.cpp
//  practice
//
//  Created by Nadeen Mohamed on 5/1/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Beast1.hpp"
#include <iostream>
using namespace std;

Beast1:: Beast1()
{
    beast.setSize(sf::Vector2f(168, 160));
    //sf::Texture beasttexture;
    
    if (!beasttexture.loadFromFile(resourcePath() + "beast1sprite.png")) {
        cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::IntRect size(0, 0, 168, 160);

    beast.setTexture(&beasttexture);
    beast.setTextureRect(size);
    beast.setScale(5, 5);
    
    //sf::Vector2u texturesize = beasttexture.getSize();
    //texturesize.x /= 2;
    //texturesize.y /= 1;
}

void Beast1::setexplosion ()
{
    if (!beasttexture.loadFromFile(resourcePath() + "explosionBeast1.png")) {
        return EXIT_FAILURE;
    }
    
    //beasttexture.loadFromFile(resourcePath() + "explosionBeast1.png");
    beast.setTexture(&beasttexture);
}

void Beast1::setrect(sf::IntRect size) {
    beast.setTextureRect(size);
}

//void Beast1::settexturerect(int n)
//{
//beast.setTextureRect(sf::IntRect(texturesize.x * n, texturesize.y * 0, texturesize.x, texturesize.y));

//}

void Beast1::setpos(int x, int y) {
    beast.setPosition(x,y);
}

void Beast1::draw(sf::RenderWindow&window)
{
    window.draw(beast);
}

int Beast1::getX()
{
    return beast.getPosition().x;
}

int Beast1::getY() {
    return beast.getPosition().y;
    
}

void Beast1::movebeast(int x,int y)
{
    beast.move(x, y);
}


bool Beast1::checkcoll(bullet bullet)
{
if ((bullet.getRight() > beast.getPosition().x) && (bullet.getTop() < beast.getPosition().y + beast.getSize().y)
&& (bullet.getBottom() > beast.getPosition().y) )
return true;
else return false;
}




