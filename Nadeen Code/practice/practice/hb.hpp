//
//  hb.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef hb_hpp
#define hb_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
#include <stdlib.h>
#include "ResourcePath.hpp"

class hb{
private:
    sf::Texture bartexture;
public:
    hb();
    sf::RectangleShape bar;
    void setrect(sf::IntRect size);
    void setpos(int x, int y);
    void draw(sf::RenderWindow&window);
    


};

#endif /* hb_hpp */
