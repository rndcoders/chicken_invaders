//
//  bullet.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/24/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef bullet_hpp
#define bullet_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"
#include <vector>


class bullet {
private:
    sf::Texture def;
    sf::Texture blast;
    sf::Texture beam;
    sf::Texture fork;
    
    
    
public:
    sf::RectangleShape bull;
    bullet();
    void draw(sf::RenderWindow& window);
    void setpos(int x,int y);
    void fire();
    void handle_events(sf::Event event, bool& fire);
    int getLeft();
    int getTop();
    int getBottom();
    int getRight();
    bool isFire(bool& fire);
    void changeWeapon();
    
};


#endif /* bullet_hpp */
