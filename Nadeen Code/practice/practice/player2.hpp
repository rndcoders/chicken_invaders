//
//  player2.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/24/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef player2_hpp
#define player2_hpp

#include <SFML/Graphics.hpp>
#include <stdio.h>
#include "ResourcePath.hpp"

class player2{
private:
    sf::Texture spaceship2;
    sf::Texture explosion2;
    bool dead=false;
    int lives=3;
    
    
public:
    sf::RectangleShape ss2;
    sf::RectangleShape exp;
    player2();
    void move(int x, int y);
    void draw(sf::RenderWindow& window);
    void setrect(sf::IntRect size);
    void handle_events(sf::Event event);
    int getx();
    int gety();
    int getLeft();
    int getTop();
    int getBottom();
    int getRight();
    void destroy(sf::RenderWindow& window);
    void checkLives();
    bool checkDead();
    
};


#endif /* player2_hpp */
