
//
// Disclaimer:
// ----------
//
// This code will work only if you selected window, graphics and audio.
//
// Note that the "Run Script" build phase will copy the required frameworks
// or dylibs to your application bundle so you can execute it on any OS X
// computer.
//
// Your resource files (images, sounds, fonts, ...) are also copied to your
// application bundle. To get the path to these resources, use the helper
// function `resourcePath()` from ResourcePath.hpp
//

#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"
#include "player1.hpp"
#include "player2.hpp"
#include "bullet.hpp"
#include "bullet2.hpp"
#include "rocks.hpp"
#include "Chicken.hpp"
#include "wep.hpp"
#include "powerup.hpp"
#include "Beast1.hpp"
#include "Beast2.hpp"
#include "BeastF1.hpp"
#include "BeastF2.hpp"
#include "eggs.hpp"
#include "hb.hpp"

using namespace std;

sf::Texture bullet_t;


//dina

vector<eggs>eggsvec;

void dropeggs(sf::RenderWindow &window, Chicken & c, bool& flag);
void dropeggsB1(sf::RenderWindow &window, Beast1& beast);
void dropeggsf1(sf::RenderWindow &window, BeastF1& f1);
void dropeggsf2(sf::RenderWindow &window, BeastF2& f2);
void dropeggsB2(sf::RenderWindow &window, Beast2& beast2);


void CHbeast1(sf::RenderWindow &window, Beast1& beast, hb& bar, int &nocollisions);
void CHbeast2(sf::RenderWindow &window, Beast2& beast2, hb& bar, int &nocollisions);//, Bullet& bullet
void CHf1(sf::RenderWindow &window, BeastF1& f1, hb& bar, int &nocollisions);
void CHf2(sf::RenderWindow &window, BeastF2& f2, hb& bar, int &nocollisions);

void movebeast1(Beast1& beast, sf::RenderWindow& window);
void movef1(BeastF1& f1);
void movef2(BeastF2& f2);


sf::IntRect sizebeast(0, 0, 168, 160);
sf::IntRect sizebeast2(0, 0, 227, 243);
sf::IntRect sizef1(0, 0, 227, 243);
sf::IntRect sizef2(0, 0, 249, 267);
sf::IntRect sizebar(0, 0, 204, 30);
sf::IntRect sizeegg(0, 0, 47, 34);


sf::Clock cB;
sf::Clock cC;

sf::Clock cE;
sf::Clock cF;
sf::Clock cG;
sf::Clock cH;
sf::Clock cI;

sf::Texture EGG;

//end of dina



sf::Texture bull;
sf::Texture bull2;
sf::Texture r;
sf::Texture beam;





using namespace std;

// Here is a small helper for you! Have a look.

//nadeen
void shoot(bool& fire, vector<bullet>& bvec, sf::RenderWindow& window, player1& sp1) {
    
   /* if (!bull.loadFromFile(resourcePath() + "bullet.png")) {
        return EXIT_FAILURE; }

    if (!beam.loadFromFile(resourcePath() + "beam.png")) {
        return EXIT_FAILURE; }*/
    
    if(fire==true && bvec.size() < 2) {
        bullet newb;
        //newb.changeWeapon();
        newb.bull.setTexture(&bullet_t);
        newb.setpos(sp1.getx()+80, sp1.gety()+130);
        bvec.push_back(newb);
        fire=false;
    }
    
    
    for(int i=0; i<bvec.size(); i++) {
        bvec[i].draw(window);
        bvec[i].fire();
        
    }
    
}

void shoot2(bool& fire2, vector<bullet2>& bvec2, sf::RenderWindow& window, player2& sp2 ){
    
    
   /* if (!bull2.loadFromFile(resourcePath() + "bullet2.png")) {
        return EXIT_FAILURE; }*/

    
    if(fire2==true && bvec2.size() < 2) {
        bullet2 newb2;
        newb2.bull.setTexture(&bull2);
        newb2.setpos(sp2.getx()+80, sp2.gety()+130);
        bvec2.push_back(newb2);
        fire2=false;
    }
    
    
    for(int i=0; i<bvec2.size(); i++) {
        bvec2[i].draw(window);
        bvec2[i].fire();
        
    }
 


}

void collide_rocksp1(player1& sp1, rocks& rock,sf::Sound& exp) {
    if(sp1.ss.getGlobalBounds().intersects(rock.r1.getGlobalBounds()) ||sp1.ss.getGlobalBounds().intersects(rock.r2.getGlobalBounds()) || sp1.ss.getGlobalBounds().intersects(rock.r3.getGlobalBounds()) ) {
        //exp.play();
        sp1.checkLives();
        

    }
    

    
}

void collide_rocksp2(player2& sp2, rocks& rock,sf::Sound& exp) {
    if(sp2.ss2.getGlobalBounds().intersects(rock.r1.getGlobalBounds()) ||sp2.ss2.getGlobalBounds().intersects(rock.r2.getGlobalBounds()) || sp2.ss2.getGlobalBounds().intersects(rock.r3.getGlobalBounds()) ) {
        //exp.play();
        sp2.checkLives();
        
    }
    
    
    
}

void collide_chicksp1bull(Chicken& chick, vector<bullet>& bvec) {
    
    for(int j=0;j<bvec.size(); j++) {
    if(chick.chicken.getGlobalBounds().intersects(bvec[j].bull.getGlobalBounds())) {
        
        //cout<< "collide" <<endl;
        chick.dead=true;
    }
        
    }
}

void collide_chicksp2bull(Chicken& chick, vector<bullet2>& bvec2) {
    
    for(int j=0;j<bvec2.size(); j++) {
        if(chick.chicken.getGlobalBounds().intersects(bvec2[j].bull.getGlobalBounds())) {
            //chick.chicken.setPosition(478382, 382829);
            //cout<< "collide" <<endl;
            chick.dead=true;
        }
        
    }
}


void pickup_drop(vector<wep>& wepvec, Chicken chickArray[10], sf::RenderWindow& window) {
    
    
    int x; //Propability the chicken will drop a gift is 1/2
    x=rand()%2;
    
    for(int i=0; i<10; i++) {
        if(chickArray[i].checkDead()==true) {
            switch(x) {
                case 0: {
                    wep newgift;
                    newgift.setPos(chickArray[i].getX()+94, chickArray[i].getY()+81);
                    wepvec.push_back(newgift);
                    x=1;
                    
                    for(int i=0; i<wepvec.size(); i++) {
                        wepvec[i].draw(window);
                        wepvec[i].move();
                        
                        
                    
                        
                    }
                    
                    
               }
            }
            
            
        }
    }
    
    
    
    
}











int main() {
    
    
    if(!bullet_t.loadFromFile(resourcePath()+"bullet.png"))
        std::cout << "Error" << std::endl;
    
    
    
    sf::Clock clock;
    sf::Clock clock2;
    sf::Clock clock3;
    sf::Clock c;
    sf::Clock cA;
    sf::Clock cD;
    sf::Clock chickClock;
    
    sf::Time t= sf::seconds(2.0);
    


    // Create the main window
    sf::RenderWindow window(sf::VideoMode(1400, 1200), "game zeft" , sf::Style::Fullscreen ); //sf::Style::Default || , sf::Style::Fullscreen
    

    
    //dina
    
    
    //sf::Texture beasttexture;
    
   // if (!beasttexture.loadFromFile(resourcePath() + "beast1sprite.png")) {
        //cout<<"error";
        //return EXIT_FAILURE;
   // }

    
    Chicken blue;
    Beast1 beast;
    Beast2 beast2;
    BeastF1 f1;
    BeastF2 f2;
    //eggs egg;
    
    int n = 0;
    
    
    sf::IntRect sizebeast(0, 0, 168, 160);
    
    //end of dina
    
    sf::Texture background;
    if (!background.loadFromFile(resourcePath() + "background.jpg")) {
        return EXIT_FAILURE; }
    
    
   
    if (!EGG.loadFromFile(resourcePath() + "eggsprite.png")) {
        return EXIT_FAILURE; }
    
    
    sf::Sprite sprite;
    sprite.setTexture(background);
    sprite.setScale(2, 2);
    
    
    srand(unsigned(time(NULL)));
    
    
    sf::IntRect s(0,0,319,372);
    sf::IntRect expsize(0,0,266,266);
    sf::IntRect size(0,0,640,640);
    
    sf::IntRect s2(0,0,188,163);
    
    
    
    player1 sp1;
    player2 sp2;
    bullet b;
    b.changeWeapon();
    bullet2 b2;
    vector <bullet> bvec;
    vector<bullet2> bvec2;
    rocks rock;
    rocks rock2;
    rocks rock3;
    vector <rocks> rockvec;
    vector <wep> wepvec;
    rocks newrock;
    
    Chicken chickArray[10];
    wep pick;
    powerup p;
    
    
    
    sf::SoundBuffer expbuff;
    if (!expbuff.loadFromFile(resourcePath() + "explosion.wav")) {
        return EXIT_FAILURE;
    }
    sf::Sound exp;
    exp.setBuffer(expbuff);
    //exp.play();
    
    
    sf::SoundBuffer pewbuff;
    if (!pewbuff.loadFromFile(resourcePath() + "pew.wav")) {
        return EXIT_FAILURE;
    }
    sf::Sound pew;
    pew.setBuffer(pewbuff);
    //pew.play();
    
    
    /*
    sf::SoundBuffer beambuff;
    if (!beambuff.loadFromFile(resourcePath() + "beam.wav")) {
        return EXIT_FAILURE;
    }
    
    sf::Sound beam;
    pew.setBuffer(beambuff);
    
    */
    
  
    // Create a graphical text to display
    sf::Font font;
    if (!font.loadFromFile(resourcePath() + "font.ttf")) {
        return EXIT_FAILURE;
    }
    sf::Text text("Chicken Invaders", font, 100);
    text.setFillColor(sf::Color::White);
    text.setPosition(900,0);
    text.setOrigin(0,0);
    

    sf::Text lose("PLAYER ONE LOSES!", font, 50);
    lose.setFillColor(sf::Color::White);
    lose.setPosition(2000,1500);
    lose.setOrigin(0,0);
    
    sf::Text lose2("PLAYER TWO LOSES!", font, 50);
    lose2.setFillColor(sf::Color::White);
    lose2.setPosition(30,1500);
    lose2.setOrigin(0,0);

    
    
    
    
    bool fire=false, fire2=false;
    bool flag=false;
    bool FLAG=false;
    
    bool moveFlag=false;
    
    int lol=rand()%11;
    cout<<lol<<endl;
    
    switch(lol) {
        case 0: flag=true;
    }

    window.setFramerateLimit(20);
    
    // Start the game loop
    while (window.isOpen())
    {
        sf:: Event event;
        

        while (window.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed :
                    window.close();
                    break;
                    
                    
                case sf::Event::KeyPressed : {
                    sp1.handle_events(event);
                    sp2.handle_events(event);
                    b.handle_events(event, fire);
                    b2.handle_events(event, fire2);
                    //if(b.isFire(fire)) {
                        //pew.play();
                        //beam.play();
                    //}
                    //if(b2.isFire(fire2)) pew.play();
                    
                    
                }
                
                    if(sf::Keyboard::isKeyPressed(sf::Keyboard::T)) {
                        FLAG=true;
                   }
                    
                    if(sf::Keyboard::isKeyPressed(sf::Keyboard::M)) {
                         movebeast1(beast,window);
                    }
                    
                    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
                        window.close();
                    }
                    
            }
            
            
          
        }
        
    
        //dina animation
     
        
        // beast1 animation
        if (cA.getElapsedTime().asSeconds() > 1) {
            
            bool minus=true;
            
            if (sizebeast.left ==168) minus=true;
            if (sizebeast.left == 0) minus=false;
            
            if(minus==true) sizebeast.left -=168;
            else sizebeast.left +=168;
            
            beast.setrect(sizebeast);
            
            cA.restart();
            
            
            //cA.restart();
        }
        
        
        // beast 2 animation
    
        
        if (cD.getElapsedTime().asMilliseconds() >= 5000) {
            
            bool minus2=true;
            
            if (sizebeast2.left ==454) minus2=true;
            if (sizebeast2.left == 0) minus2=false;
            
            if(minus2==true) sizebeast2.left -=227;
            else sizebeast2.left +=227;

            
            /*
            sizebeast2.left += 227;
            beast2.setrect(sizebeast2);
            
            if (sizebeast2.left == 454) sizebeast2.left = 0;
             */
            
            beast2.setrect(sizebeast2);
            
            cD.restart();
           

        }
        
        // Final beast 1 animation
        if (cB.getElapsedTime().asMilliseconds() >= 5000) {
            
            sizef1.left += 227;
            
            f1.setrect(sizef1);
            
            if (sizef1.left == 454 ) sizef1.left = 0;
            
            
            
            cB.restart();
        }
        
        //Final beast 2 animation
        
        if (cC.getElapsedTime().asMilliseconds() >= 5000) {
            
            sizef2.left += 249;
            f2.setrect(sizef2);
            
            if (sizef2.left == 498) sizef2.left = 0;
            
            
            
            cC.restart();
        }
        
        

        
        //end of dina animation
        
        
        
        
        //Animation
        
        
        
        if (clock.getElapsedTime().asSeconds() > 0.2f){
            bool minus=true;
            
            if (size.left ==2560) minus=true;
            if (size.left == 0) minus=false;
            
            if(minus==true) size.left -=640;
            else size.left +=640;
            
            sp1.setrect(size);
            sp2.setrect(size);
           
            // expsize.left +=266;
            // if(expsize.left==4522) sp1.exp.setPosition(372873, 372873);
            // sp1.exp.setTextureRect(expsize);

            clock.restart();
            
        }
        
        
        if (clock2.getElapsedTime().asSeconds() > 0.3f){
        
            
            s.left +=319;
            if(s.left==2233) s.left=0;
            rock.setrect(s);
            newrock.setrect(s);
            //rock2.setrect(s);
            //rock3.setrect(s);
            clock2.restart();
            
        }
        
        
        
        if (clock3.getElapsedTime().asSeconds() > 0.1f){
            for(int i=0; i<10; i++) {
            bool minus2;
            
            if (s2.left ==752) minus2=true;
            if (s2.left == 0) minus2=false;
            
            if(minus2==true) s2.left -=188;
            else s2.left +=188;
            
            chickArray[i].setrect(s2);
                        
            clock3.restart();
            }
        }
        
        
        
        //End of Animation
        
       
        
        
      
     
        // Clear screen
        window.clear();
        window.draw(sprite);
        
        
        
        
        
        /*
        if(FLAG==true) {
            eggs newegg;
            sizeegg.left=0;
            newegg.egg.setTexture(&EGG);
            newegg.egg.setTextureRect(sizeegg);
            newegg.setpos(chickArray[4].getX()+94, chickArray[4].getY()+86);
            eggsvec.push_back(newegg);
            FLAG=false;
        }
        
        for (int i = 0; i < eggsvec.size(); i++)
        {
            //if (eggsvec[i].getY() <1400)//the 400 will change according to the window
            //{
            
            eggsvec[i].draw(window);
            eggsvec[i].moveegg(2);
        
        }
         */
        
        

        
        
         //Killing chickens
        for(int i=0; i<10; i++) {
            collide_chicksp1bull(chickArray[i], bvec);
            collide_chicksp2bull(chickArray[i], bvec2);
        }
        
        //Dropping eggs
        
        
        
        //Moving rock
        
        
        
         //if (!r.loadFromFile(resourcePath() + "rock1sprite.png")) {
         //return EXIT_FAILURE; }

        bool flag=true;
        if(flag==true && rockvec.size() < 2) {
        
        newrock.r1.setTexture(&r);
        rockvec.push_back(newrock);
        
        }
    
        
        for(int i=0; i<rockvec.size() && flag==true; i++) {
            rockvec[i].draw(window);
            rockvec[i].move(5, 5);
            flag=false;
        }

        

       /*
        
        //Shooting rocks
        for(int i=0; i<bvec.size() ; i++) {
            rock.checkCollision(bvec[i]);
            //if(rock.isShot()) exp.play();
        }
        
        
        
        for(int i=0; i<bvec2.size() ; i++) {
            rock.checkCollision(bvec2[i]);
            //if(rock.isShot()) exp.play();
        }
        
       */

        shoot(fire, bvec, window, sp1);
        shoot2(fire2,bvec2, window, sp2);
        
        pickup_drop(wepvec, chickArray, window);
        
        
        
        
        
        //collide_rocksp1(sp1, rock, exp);
        //collide_rocksp2(sp2, rock, exp);
        
        //Collecting gifts
        
        
        /*
        int x; //Propability the chicken will drop a gift is 1/2
        x=rand()%2;
        
        for(int i=0; i<10; i++) {
            if(chickArray[i].checkDead()==true) {
                switch(x) {
                    case 0: {
                        wep newgift;
                        newgift.setPos(chickArray[i].getX()+94, chickArray[i].getY()+81);
                        wepvec.push_back(newgift);
                        x=1;
                        
                        for(int i=0; i<wepvec.size(); i++) {
                            wepvec[i].draw(window);
                            wepvec[i].move();
                            
                        }
                        
                        
                    }
                }
                
                
            }
        }
        
        */
        
        
        
    
        
        // Draw the string
        
        

        window.draw(text);
        if(sp1.checkDead()==true) {
            window.draw(lose);
            
        }
        
        if(sp2.checkDead()==true) {
        window.draw(lose2);
        }
       
        
        for(int i=0; i<10; i++) {
            if(chickArray[i].dead==false) {
            chickArray[i].setpos(i*chickArray[i].chicken.getSize().y +300,100);
            chickArray[i].draw(window);
            }
            else {
                chickArray[i].chicken.setPosition(478382, 382829);
            }
        }
        
        
        sp1.draw(window);
        sp2.draw(window);
        //rock.draw(window);
        
        pick.setPos(0,0);
        //pick.draw(window);
        //p.draw(window);
        
        
        
        //dina draw
        
        
        
        //if(chickClock.getElapsedTime().asSeconds()> t.asSeconds()) {
            
           //if(FLAG==true) {
                
                dropeggs(window, chickArray[2], FLAG);
            //}
            
            
            //chickClock.restart();
        //}
 
        beast2.setpos(200,200);
        beast.setpos(0,0);
        f1.setpos(400,400);
        f2.setpos(600,600);
        //beast.draw(window);
        //beast2.draw(window);
        //f1.draw(window);
        //f2.draw(window);
        
       
        //end of dina draw
        // Update the window
        window.display();
    }
    


    return EXIT_SUCCESS;
}


void dropeggs(sf::RenderWindow &window, Chicken& c, bool& flag)
{
    
  //  if (!EGG.loadFromFile(resourcePath() + "eggsprite.png")) {
  //      return EXIT_FAILURE; }
    
    sf::IntRect size(0, 0, 47, 34);
    
    //eggs newegg;
    if(flag==true) {
        eggs newegg;
        size.left=0;
        newegg.egg.setTexture(&EGG);
        newegg.egg.setTextureRect(size);
    newegg.setpos(c.getX()+94, c.getY()+86);
    eggsvec.push_back(newegg);
    flag=false;
    }
    
    for (int i = 0; i < eggsvec.size(); i++)
    {
        if (eggsvec[i].getY() <1400)//the 400 will change according to the window
        {

        eggsvec[i].draw(window);
        eggsvec[i].moveegg(2);
        
            
            
            
            
        }
        //else if( collide with spaceship) create bool, spaceship destroyed ,egg disappears
        else {
            size.left+=47;
           eggsvec[i].setrect(size);
           eggsvec[i].draw(window);
            
            
            cE.restart();
            if (cE.getElapsedTime().asMilliseconds() >= 100) {
                
                eggsvec[i].setpos(2345, 87478);
                
                cE.restart();
            }
            
   
            //sound of cracked
        }
    }
}



// function for dropping eggs from beast1

void dropeggsB1(sf::RenderWindow &window, Beast1& beast)
{
    
    eggs newegg;
    newegg.setpos(beast.getX(), beast.getY());
    eggsvec.push_back(newegg);
    
    
    for (int i = 0; i < eggsvec.size(); i++)
    {
        if (eggsvec[i].getY() <1300)//the 400 will change according to the window
        {
            
            sizeegg.left = 0;
            eggsvec[i].setrect(sizeegg);
            eggsvec[i].draw(window);
            eggsvec[i].moveegg(3);
        }
        //else if( collide with spaceship) create bool, spaceship destroyed ,egg disappears
        else {
            sizeegg.left += 47;
            eggsvec[i].setrect(sizeegg);
            eggsvec[i].draw(window);
            if (cF.getElapsedTime().asMilliseconds() >= 500) {
                
                eggsvec[i].setpos(2345, 6783);
                
                cF.restart();
            }
            //sound of cracked
        }
    }
}

// function for dropping eggs from beast 2

void dropeggsB2(sf::RenderWindow &window, Beast2& beast2)
{
    eggs newegg;
    newegg.setpos(beast2.getX(), beast2.getY());
    eggsvec.push_back(newegg);
    
    for (int i = 0; i < eggsvec.size(); i++)
    {
        if (eggsvec[i].getY() <1300)//the 400 will change according to the window
        {
            
            sizeegg.left = 0;
            eggsvec[i].setrect(sizeegg);
            eggsvec[i].draw(window);
            eggsvec[i].moveegg(3);
        }
        //else if( collide with spaceship) create bool, spaceship destroyed ,egg disappears
        else {
            sizeegg.left += 47;
            eggsvec[i].setrect(sizeegg);
            eggsvec[i].draw(window);
            if (cG.getElapsedTime().asMilliseconds() >= 500) {
                
                eggsvec[i].setpos(2364, 7467);
                
                cG.restart();
            }
            //sound of cracked
        }
    }
}



// function for dropping eggs from final beast 1

void dropeggsf1(sf::RenderWindow &window, BeastF1& f1)
{
    
    eggs newegg;
    newegg.setpos(f1.getX(), f1.getY());
    eggsvec.push_back(newegg);
    
    for (int i = 0; i < eggsvec.size(); i++)
    {
        if (eggsvec[i].getY() < 1300)//the 400 will change according to the window
        {
            
            sizeegg.left = 0;
            eggsvec[i].setrect(sizeegg);
            eggsvec[i].draw(window);
            eggsvec[i].moveegg(3);
        }
        //else if( collide with spaceship) create bool, spaceship destroyed ,egg disappears
        else {
            sizeegg.left += 47;
            eggsvec[i].setrect(sizeegg);
            eggsvec[i].draw(window);
            if (cH.getElapsedTime().asMilliseconds() >= 500) {
                
                eggsvec[i].setpos(2345, 5432);
                
                cH.restart();
            }
            //sound of cracked
        }
    }
}



// function for dropping eggs from beast1

void dropeggsf2(sf::RenderWindow &window, BeastF2& f2)
{
    
    eggs newegg;
    newegg.setpos( f2.getX(), f2.getY() );
    eggsvec.push_back(newegg);
    
    for (int i = 0; i < eggsvec.size(); i++)
    {
        if (eggsvec[i].getY() <1300)//the 400 will change according to the window
        {
            
            sizeegg.left = 0;
            eggsvec[i].setrect(sizeegg);
            eggsvec[i].draw(window);
            eggsvec[i].moveegg(3);
        }
        //else if( collide with spaceship) create bool, spaceship destroyed ,egg disappears
        else {
            sizeegg.left += 47;
            eggsvec[i].setrect(sizeegg);
            eggsvec[i].draw(window);
            if (cI.getElapsedTime().asMilliseconds() >= 500) {
                
                eggsvec[i].setpos(2345, 7903);
                
                cI.restart();
            }
            //sound of cracked
        }
    }
}






// function for number of collisions everytime bullet is fired in main
//if ((beast1.checkcoll(bullet)) nocollisions++;

void CHbeast1(sf::RenderWindow &window, Beast1& beast, hb& bar, int &nocollisions)//, Bullet& bullet
{
    
    if (nocollisions <= 5)
    {
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 5) && (nocollisions <= 10))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 10) && (nocollisions <= 15))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 15) && (nocollisions <= 20))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    
    
    else if (nocollisions == 21)
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
        beast.setexplosion();
        beast.draw(window);
        beast.setpos(23456,78990);
        beast.draw(window);
        
    }
}

// function for checking health of beast 2

void CHbeast2(sf::RenderWindow &window, Beast2& beast2, hb& bar, int &nocollisions)//, Bullet& bullet
{
    
    if (nocollisions <= 7)
    {
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 7) && (nocollisions <= 14))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 14) && (nocollisions <= 20))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 20) && (nocollisions <= 26))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    
    
    else if (nocollisions == 27)
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
        beast2.setexplosion();
        beast2.draw(window);
        beast2.setpos(23456, 78990);
        beast2.draw(window);
        
    }
}




void CHf1(sf::RenderWindow &window, BeastF1& f1, hb& bar, int &nocollisions)//, Bullet& bullet
{
    
    if (nocollisions <= 9)
    {
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 9) && (nocollisions <= 18))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 18) && (nocollisions <= 27))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 27) && (nocollisions <= 32))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    
    
    else if (nocollisions == 33)
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
        f1.setexplosion();
        f1.draw(window);
        f1.setpos(23456, 78990);
        f1.draw(window);
        
    }
}


void CHf2(sf::RenderWindow &window, BeastF2& f2, hb& bar, int &nocollisions)//, Bullet& bullet
{
    
    if (nocollisions <= 9)
    {
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 9) && (nocollisions <= 18))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 18) && (nocollisions <= 27))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    else if ((nocollisions > 27) && (nocollisions <= 32))
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
    }
    
    
    
    else if (nocollisions == 33)
    {
        sizebar.left += 204;
        bar.setrect(sizebar);
        bar.draw(window);
        f2.setexplosion();
        f2.draw(window);
        f2.setpos(23456, 78990);
        f2.draw(window);
        
    }
}




//random movement for final beast 1

void movef1(BeastF1&f1)
{
    
    int x;
    int y;
    int xlimit = 700; // this number changed according to x coordinate of mid screen;
    int ylimit = 1000; // this number is the y coordinate of mid screen
    int r1, r2;
    
    r1 = rand() % 2;
    r2 = rand() % 2;
    
    
    if (r1 == 0) x = -3;
    else x = 3;
    
    if (r2 == 0) y = 3;
    else (y = -3);
    
    while ((f1.getX() + x > xlimit) || (f1.getY() + y > ylimit) || (f1.getX() + x <0 ) || (f1.getY() + y < 0) )
    {
        r1 = rand() % 2;
        r2 = rand() % 2;
        if (r1 == 0) x = -3;
        else x = 3;
        
        if (r2 == 0) y = 3;
        else (y = -3);
        
    }
    
    f1.movebeast(x, y);
    
}


// random movement for beast 2

void movebeast2(Beast2&beast2)
{
    
    int x;
    int y;
    int xlimit = 700; // this number changed according to x coordinate of mid screen;
    int ylimit = 1000; // this number is the y coordinate of mid screen
    int r1, r2;
    
    r1 = rand() % 2;
    r2 = rand() % 2;
    
    
    if (r1 == 0) x = -3;
    else x = 3;
    
    if (r2 == 0) y = 3;
    else (y = -3);
    
    while ((beast2.getX() + x > xlimit) || (beast2.getY() + y > ylimit) || (beast2.getX() + x <0) || (beast2.getY() + y < 0))
    {
        r1 = rand() % 2;
        r2 = rand() % 2;
        if (r1 == 0) x = -3;
        else x = 3;
        
        if (r2 == 0) y = 3;
        else (y = -3);
        
    }
    
    beast2.movebeast(x, y);
    
}



//random movement for final beast 2

void movef2 (BeastF2&f2)
{
    
    int x;
    int y;
    int xlimit = 700; // this number changed according to x coordinate of mid screen;
    int ylimit = 1000; // this number is the y coordinate of  screen
    int r1, r2;
    
    r1 = rand() % 2;
    r2 = rand() % 2;
    
    
    if (r1 == 0) x = -3;
    else x = 3;
    
    if (r2 == 0) y = 3;
    else (y = -3);
    
    while ( (f2.getX() + x < xlimit) || (f2.getY() + y < ylimit)  || (f2.getX() + x < 0 ) || (f2.getY() + y < 0 ) ) 
    {
        r1 = rand() % 2;
        r2 = rand() % 2;
        if (r1 == 0) x = -3;
        else x = 3;
        
        if (r2 == 0) y = 3;
        else (y = -3);
        
    }
    
    f2.movebeast(x, y);
    
}

//random movement for beast 1


void movebeast1 (Beast1& beast, sf::RenderWindow& window)
{
    
    int x;
    int y;
    int xlimit = 1400; // this number changed according to x coordinate of screen;
    int ylimit = 1000; // this number is the y coordinate of  screen
    int r1, r2;
    
     r1 = rand() % 1401;
    r2 = rand() % 1401;
    
    
    if (r1 == 0) x = -300;
   else x = 300;
    
    if (r2 == 0) y = 300;
    else (y = -300);
    
    while ((beast.getX() + x > xlimit) || (beast.getY() + y > ylimit) || (beast.getX() + x <0 )  || (beast.getY() + y < 0) )
    {
        r1 = rand() % 2;
        r2 = rand() % 2;
        if (r1 == 0) x = -300;
        else x = 300;
        
        if (r2 == 0) y = 300;
        else (y = -300);
        
    }
    
    beast.movebeast(x, y);
    beast.draw(window);
}


