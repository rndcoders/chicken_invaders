#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <iostream>
#include<cstdlib>
#include<string>
#include <random>
#include <time.h>
#include <string>
#include "SetStartMenu.h"

using namespace sf;
using namespace std;


void SetStartMenu::initialize(Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue) {


	if (!texture.loadFromFile(resourcePath() + "cover.jpg")) {
		cout << "error cover photo" << endl;
	}
	sprite.setTexture(texture);
	

	if (!Fire1Texture.loadFromFile(resourcePath() +"Fire1.png")) {
		cout << "error cover photo" << endl;
	}
	Fire1Sprite.setTexture(Fire1Texture);


	if (!Fire2Texture.loadFromFile(resourcePath() + "Fire2.png")) {
		cout << "error cover photo" << endl;
	}
	Fire2Sprite.setTexture(Fire2Texture);


	if (!Fire3Texture.loadFromFile(resourcePath() + "Fire3.png")) {
		cout << "error cover photo" << endl;
	}
	Fire3Sprite.setTexture(Fire3Texture);


	if (!Fire4Texture.loadFromFile(resourcePath() + "Fire4.png")) {
		cout << "error cover photo" << endl;
	}
	Fire4Sprite.setTexture(Fire4Texture);



	if (!Fire5Texture.loadFromFile(resourcePath() + "Fire5.png")) {
		cout << "error cover photo" << endl;
	}
	Fire5Sprite.setTexture(Fire5Texture);

	if (!Fire6Texture.loadFromFile(resourcePath() + "Fire6.png")) {
		cout << "error cover photo" << endl;
	}
	Fire6Sprite.setTexture(Fire6Texture);


	if (!Fire7Texture.loadFromFile(resourcePath() + "Fire7.png")) {
		cout << "error cover photo" << endl;
	}
	Fire7Sprite.setTexture(Fire7Texture);


	if (!Fire8Texture.loadFromFile(resourcePath() + "Fire8.png")) {
		cout << "error cover photo" << endl;
	}
	Fire8Sprite.setTexture(Fire8Texture);


	if (!Fire9Texture.loadFromFile(resourcePath() + "Fire9.png")) {
		cout << "error cover photo" << endl;
	}
	Fire9Sprite.setTexture(Fire9Texture);


	if (!Fire10Texture.loadFromFile(resourcePath() + "Fire10.png")) {
		cout << "error cover photo" << endl;
	}
	Fire10Sprite.setTexture(Fire10Texture);

	if (!Fire11Texture.loadFromFile(resourcePath() + "Fire11.png")) {
		cout << "error cover photo" << endl;
	}
	Fire11Sprite.setTexture(Fire11Texture);

	if (!Fire12Texture.loadFromFile("resourcePath() + Fire12.png")) {
		cout << "error cover photo" << endl;
	}
	Fire12Sprite.setTexture(Fire12Texture);

	if (!Fire13Texture.loadFromFile(resourcePath() + "Fire13.png")) {
		cout << "error cover photo" << endl;
	}
	Fire13Sprite.setTexture(Fire13Texture);


	//postion of the explosion fire


	//new game shape
	NewGameRect.setSize(Vector2f(600, 100));
	NewGameRect.setPosition(100, 200);
	NewGameRect.setOutlineThickness(4);
	NewGameRect.setOutlineColor(Blue2);

	//Loadgame shape
	LoadGameRect.setSize(Vector2f(600, 100));
	LoadGameRect.setPosition(100, 350);
	LoadGameRect.setOutlineThickness(4);
	LoadGameRect.setOutlineColor(Blue2);
	LoadGameRect.setFillColor(LightBlue);

	//Settings shape
	SettingsRect.setSize(Vector2f(600, 100));
	SettingsRect.setPosition(100, 500);
	SettingsRect.setOutlineThickness(4);
	SettingsRect.setOutlineColor(Blue2);
	SettingsRect.setFillColor(LightBlue);


	//Exit shape
	ExitRect.setSize(Vector2f(600, 100));
	ExitRect.setPosition(100, 650);
	ExitRect.setOutlineThickness(4);
	ExitRect.setOutlineColor(Blue2);




	//text
	if (!font.loadFromFile(resourcePath() + "fontt.ttf")) {
		cout << "font error " << endl;
	}
	if (!font2.loadFromFile(resourcePath() + "sansation.ttf")) {
		cout << "sansation font error " << endl;
	}


	//Headline text
	/*
	textHeadline.setFont(font);
	textHeadline.setString("chicken invaders");
	textHeadline.setCharacterSize( 50 );
	textHeadline.setFillColor(Blue2);
	textHeadline.move(200, 10);
	*/

	// start game text
	textStartGame.setFont(font2);
	textStartGame.setString("New Game");
	textStartGame.setCharacterSize(60);
	textStartGame.setFillColor(Blue2);
	textStartGame.move(260, 210);



	// LoadGame text
	textLoadGame.setFont(font2);
	textLoadGame.setString("Load Game");
	textLoadGame.setCharacterSize(60);
	textLoadGame.setFillColor(Blue2);
	textLoadGame.move(260, 360);


	// Settings text
	textSettings.setFont(font2);
	textSettings.setString("Settings");
	textSettings.setCharacterSize(60);
	textSettings.setFillColor(Blue2);
	textSettings.move(285, 510);


	// Exit text
	textExit.setFont(font2);
	textExit.setString("Exit");
	textExit.setCharacterSize(60);
	textExit.setFillColor(Blue2);
	textExit.move(340, 660);



	//Credits

	Credits.setFont(font2);
	Credits.setString("Designed By /n seif");
	Credits.setCharacterSize(40);
	Credits.setFillColor(Blue2);
	Credits.move(340, 660);


}


void SetStartMenu::render(RenderWindow &Window) {

	Window.draw(sprite);
	Window.draw(NewGameRect);
	Window.draw(LoadGameRect);
	Window.draw(SettingsRect);
	Window.draw(ExitRect);
	Window.draw(textHeadline);
	Window.draw(textStartGame);
	Window.draw(textLoadGame);
	Window.draw(textSettings);
	Window.draw(textExit);

	//Window.draw(Credits);



}




void SetStartMenu::handle(RenderWindow &Window, Vector2i &location, int &tap, Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue, bool &flag) {
	location = Mouse::getPosition(Window);



	if (NewGameRect.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
	{
		tap = 2;
		flag = true;

	}

	else if ((NewGameRect.getGlobalBounds().contains(location.x, location.y) && NewGameRect.getFillColor() != Color::Red))
	{
		NewGameRect.setFillColor(SeaBlue);
	}
	else if (NewGameRect.getFillColor() != Color::Red)
	{
		NewGameRect.setFillColor(LightBlue);
	}







	if (LoadGameRect.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
	{
		tap = 3;

	}
	else if ((LoadGameRect.getGlobalBounds().contains(location.x, location.y) && LoadGameRect.getFillColor() != Color::Red))
	{
		LoadGameRect.setFillColor(SeaBlue);
	}
	else if (LoadGameRect.getFillColor() != Color::Red)
	{
		LoadGameRect.setFillColor(LightBlue);
	}



	if (SettingsRect.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
	{
		tap = 4;

	}
	else if ((SettingsRect.getGlobalBounds().contains(location.x, location.y) && SettingsRect.getFillColor() != Color::Red))
	{
		SettingsRect.setFillColor(SeaBlue);

	}
	else if (SettingsRect.getFillColor() != Color::Red)
	{
		SettingsRect.setFillColor(LightBlue);
	}





	if (ExitRect.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
	{
		tap = 5;


	}
	else if ((ExitRect.getGlobalBounds().contains(location.x, location.y) && ExitRect.getFillColor() != Color::Red))
	{
		ExitRect.setFillColor(SeaBlue);

	}
	else if (ExitRect.getFillColor() != Color::Red)
	{
		ExitRect.setFillColor(LightBlue);
	}



}


