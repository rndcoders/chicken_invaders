//
//  wep.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/30/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef wep_hpp
#define wep_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <time.h>
#include <stdlib.h>
#include "ResourcePath.hpp"

class wep{
private:
    sf::Texture p1;
    sf::Texture p2;
    sf::Texture p3;
    
public:
    sf::RectangleShape w;
    sf::RectangleShape w1;
    sf::RectangleShape w2;
    sf::RectangleShape w3;
    wep();
    int type; //0 for fork, 1 for blast, 2 for beam
    void draw(sf::RenderWindow& window);
    void move();
    void setPos(int x, int y);
    int getLeft();
    int getTop();
    int getBottom();
    int getRight();
    

};

#endif /* wep_hpp */
