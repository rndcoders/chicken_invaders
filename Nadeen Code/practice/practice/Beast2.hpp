//
//  Beast2.hpp
//  practice
//
//  Created by Nadeen Mohamed on 5/1/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Beast2_hpp
#define Beast2_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
#include <stdlib.h>
#include "ResourcePath.hpp"
#include "bullet.hpp"


class Beast2{
private:
    sf::Texture beasttexture;
    sf::Vector2u texturesize;
    
public:
    Beast2();
    sf::RectangleShape beast;
    void setexplosion();
    //void settexturerect(int n);
    void setpos(int x, int y);
    void draw(sf::RenderWindow&window);
    void movebeast(int x, int y);
    void setrect(sf::IntRect size);
    int getX();
    int getY();
    bool checkcoll(bullet bullet);

    
};

#endif /* Beast2_hpp */
