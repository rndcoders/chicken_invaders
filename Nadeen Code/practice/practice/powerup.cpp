//
//  powerup.cpp
//  practice
//
//  Created by Nadeen Mohamed on 5/1/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "powerup.hpp"


powerup::powerup() {
    
    if (!pu.loadFromFile(resourcePath() + "powerup.png")) {
        return EXIT_FAILURE; }
    
    pow.setSize(sf::Vector2f(256,256));
    pow.setScale(0.4, 0.4);
    pow.setTexture(&pu);
    
}


void powerup::move() {
   
    
    pow.move(0,30);
    
    
}


void powerup::draw(sf::RenderWindow& window) {
        window.draw(pow);
  
}


void powerup::setPos(int x, int y) {
    
     pow.setPosition(x, y);

}

int powerup::getLeft() {
    
    return pow.getPosition().x;
  
    
}

int powerup::getTop() {
    
   return pow.getPosition().y;
  
    
}

int powerup::getRight() {
    
    return pow.getPosition().x + pow.getSize().x;
   
    
}

int powerup::getBottom() {
    
    return pow.getPosition().y + pow.getSize().y;
    
    
}
