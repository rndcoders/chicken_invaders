//
//  Egg.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Egg_hpp
#define Egg_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <string>
#include "ResourcePath.hpp"


class Egg{
private:
    sf::RectangleShape egg;
    sf::Texture eggtexture;
    sf::Vector2u texturesize;
    
public:
    Egg();
    void settexturerect(int n);
    void setpos(sf::Vector2f newPos);
    void draw(sf::RenderWindow&window);
    void moveegg(int speed);
    int getX();
    int getY();
    
};


#endif /* Egg_hpp */
