//
//  bullet2.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/24/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef bullet2_hpp
#define bullet2_hpp


#include <stdio.h>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"
#include <vector>

class bullet2 {
private:
    sf::Texture texture;
    
    
public:
    sf::RectangleShape bull;
    bullet2();
    void draw(sf::RenderWindow& window);
    void setpos(int x,int y);
    void fire();
    void handle_events(sf::Event event, bool& fire);
    int getLeft();
    int getTop();
    int getBottom();
    int getRight();
    bool isFire(bool& fire);
};

#endif /* bullet2_hpp */
