//
//  wep.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/30/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "wep.hpp"


wep::wep() {
    
    srand(unsigned(time(NULL)));
    
    int x=rand()%3; //0 for fork, 1 for blast, 2 for beam

    
    if (!p1.loadFromFile(resourcePath() + "pickup1.png")) {
        return EXIT_FAILURE; }
    
    if (!p2.loadFromFile(resourcePath() + "pickup2.png")) {
        return EXIT_FAILURE; }
    
    if (!p3.loadFromFile(resourcePath() + "pickup3.png")) {
        return EXIT_FAILURE; }

    w1.setSize(sf::Vector2f(200,200));
    w2.setSize(sf::Vector2f(200,200));
    w3.setSize(sf::Vector2f(200,200));
    
    w1.setScale(0.4, 0.4);
    w2.setScale(0.4, 0.4);
    w3.setScale(0.4, 0.4);
    
    switch(x) {
        case 0: {
            type=0;
            w1.setTexture(&p1);
        }
        case 1: {
            type=1;
            w2.setTexture(&p2);
        }
        case 2: {
            type=2;
            w3.setTexture(&p3);
        }
    }
    
    /*
    w.setSize(sf::Vector2f(200,200));
    
    switch(x) {
        case 0: {
            type=0;
            w.setTexture(&p1);
        }
        case 1: {
            type=1;
            w.setTexture(&p2);
        }
        case 2: {
            type=2;
            w.setTexture(&p3);
        }
    }
    */


}

void wep::draw(sf::RenderWindow& window) {
    
    switch(type) {
        case 0: {
            window.draw(w1);
        }
        case 1: {
            window.draw(w2);
        }
        case 2: {
            window.draw(w3);
        }
    }
    
    
    //window.draw(w);
}

void wep::move() {
    
    switch(type) {
        case 0: {
            w1.move(0,30);        }
        case 1: {
            w2.move(0,30);
        }
        case 2: {
            w3.move(0,30);
        }
    }

    //w.move(0,30);
}

void wep::setPos(int x, int y) {
    
    switch(type) {
        case 0: {
            w1.setPosition(x, y);        }
        case 1: {
            w2.setPosition(x, y);
        }
        case 2: {
            w3.setPosition(x, y);
        }
    }

    
    
    //w.setPosition(x, y);

}

int wep::getLeft() {
    
    switch(type) {
        case 0: {
            return w1.getPosition().x;
        }
        case 1: {
            return w2.getPosition().x;
        }
        case 2: {
            return w3.getPosition().x;
        }
    }


    
}

int wep::getTop() {
    
    switch(type) {
        case 0: {
            return w1.getPosition().y;
        }
        case 1: {
            return w2.getPosition().y;
        }
        case 2: {
            return w3.getPosition().y;
        }
    }
    
    

    
}

int wep::getRight() {
    
    switch(type) {
        case 0: {
            return w1.getPosition().x + w1.getSize().x;
        }
        case 1: {
            return w2.getPosition().x + w2.getSize().x;
        }
        case 2: {
            return w3.getPosition().x + w3.getSize().x;
        }
    }

    
    
    //return ss.getPosition().x + ss.getSize().x;
}

int wep::getBottom() {
    
    switch(type) {
        case 0: {
            return w1.getPosition().y + w1.getSize().y;
        }
        case 1: {
            return w2.getPosition().y + w2.getSize().y;
        }
        case 2: {
            return w3.getPosition().y + w3.getSize().y;
        }
    }

    
   // return ss.getPosition().y + ss.getSize().y;
    
}


