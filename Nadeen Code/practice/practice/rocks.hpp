//
//  rocks.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/24/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef rocks_hpp
#define rocks_hpp

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "ResourcePath.hpp"
#include <vector>
#include <stdio.h>
#include "bullet.hpp"
#include "bullet2.hpp"
#include "player1.hpp"

class rocks{
private:
    sf::Texture rock1;
    sf::Texture rock2;
    sf::Texture rock3;
    
    bool shot=false;
    
    sf::SoundBuffer buffer;
    sf::Sound exp;
    int x;
public:
    sf::RectangleShape r1;
    sf::RectangleShape r2;
    sf::RectangleShape r3;
    rocks();
    void draw(sf::RenderWindow& window);
    void setrect(sf::IntRect rect);
    void move(int x,int y);
    int getx();
    int gety();
    void setPos(int x, int y);
    int getLeft();
    int getTop();
    int getBottom();
    int getRight();
    void checkCollision(bullet bull);
    void checkCollision(bullet2 bull);
    bool isShot();
    bool isDrawn();
    

    
};
#endif /* rocks_hpp */
