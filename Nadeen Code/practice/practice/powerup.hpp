//
//  powerup.hpp
//  practice
//
//  Created by Nadeen Mohamed on 5/1/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef powerup_hpp
#define powerup_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
#include <stdlib.h>
#include "ResourcePath.hpp"


class powerup {
private:
     sf::Texture pu;
    
public:
    sf::RectangleShape pow;
    powerup();
    void draw(sf::RenderWindow& window);
    void move();
    void setPos(int x, int y);
    int getLeft();
    int getTop();
    int getBottom();
    int getRight();

    
};

#endif /* powerup_hpp */
