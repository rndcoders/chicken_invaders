//
//  SetStartMenu.hpp
//  trial
//
//  Created by SEIF on 12/7/16.
//  Copyright � 2016 SEIF. All rights reserved.
//

#ifndef SetStartMenu_h
#define SetStartMenu_h


#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include<cstdlib>
#include<string>
#include <random>
#include <time.h>
#include <string>
#include "ResourcePath.hpp"

using namespace sf;
using namespace std;

class SetStartMenu {

public:
	//void SetStartMenu() ;
	void initialize(Color, Color, Color, Color);
	void render(RenderWindow &Window);
	void handle(RenderWindow &Window, Vector2i &location, int &tap, Color, Color, Color, Color, bool& flag);
	//void CheckMouse(int &current, int &previous, Vector2i location, RenderWindow &Window);
	//bool CheckFlag();


private:
	RectangleShape NewGameRect, LoadGameRect, SettingsRect, ExitRect, mouser;
	Text textStartGame, textLoadGame, textSettings, textExit, textHeadline, Credits;
	Font font, font2;
	Texture texture, texture2;
	Sprite sprite;
	Music music;
	Sound sound;
	Texture Fire1Texture, Fire2Texture, Fire3Texture, Fire4Texture, Fire5Texture, Fire6Texture, Fire7Texture, Fire8Texture, Fire9Texture, Fire10Texture, Fire11Texture, Fire12Texture, Fire13Texture;
	Sprite Fire1Sprite, Fire2Sprite, Fire3Sprite, Fire4Sprite, Fire5Sprite, Fire6Sprite, Fire7Sprite, Fire8Sprite, Fire9Sprite, Fire10Sprite, Fire11Sprite, Fire12Sprite, Fire13Sprite;
	Time clocktime;


};



#endif /* SetStartMenu_hpp */
