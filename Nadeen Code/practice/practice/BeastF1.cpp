//
//  BeastF1.cpp
//  practice
//
//  Created by Nadeen Mohamed on 5/1/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "BeastF1.hpp"

BeastF1::BeastF1()
{
    beast.setSize(sf::Vector2f(227, 243));
        
    if (!beasttexture.loadFromFile(resourcePath() +"FB1sprite.png")) {
        return ;
    }
    
    beast.setTexture(&beasttexture);
    
    sf::IntRect size(0, 0, 227, 243);
    beast.setTextureRect(size);
    
    //sf::Vector2u texturesize = beasttexture.getSize();
    //texturesize.x /= 2;
    //texturesize.y /= 1;
}

void BeastF1::setexplosion()
{
    beasttexture.loadFromFile(resourcePath() + "explosionFB1.png");
    beast.setTexture(&beasttexture);
}

void BeastF1::setrect(sf::IntRect size) {
    beast.setTextureRect(size);
}

//void Beast1::settexturerect(int n)
//{
//beast.setTextureRect(sf::IntRect(texturesize.x * n, texturesize.y * 0, texturesize.x, texturesize.y));

//}

void BeastF1::setpos(int x, int y) {
    beast.setPosition(x, y);
}

void BeastF1::draw(sf::RenderWindow&window)
{
    window.draw(beast);
}

int BeastF1::getX()
{
    return beast.getPosition().x;
}

int BeastF1::getY() {
    return beast.getPosition().y;
    
}

void BeastF1::movebeast(int x, int y)
{
    beast.move(x, y);
}


bool BeastF1::checkcoll(bullet bullet)
{
if ((bullet.getRight() > beast.getPosition().x) && (bullet.getTop() < beast.getPosition().y + beast.getSize().y)
&& (bullet.getBottom() > beast.getPosition().y) )
return true;
else return false;
}



