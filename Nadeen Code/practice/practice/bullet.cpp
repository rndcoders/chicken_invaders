//
//  bullet.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/24/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "bullet.hpp"
#include "player1.hpp"
#include "ResourcePath.hpp"

#include <iostream>
using namespace std;



bullet::bullet() {
    
    if (!def.loadFromFile(resourcePath() + "bullet.png")) {
        return EXIT_FAILURE; }
    
    if (!beam.loadFromFile(resourcePath() + "beam.png")) {
        return EXIT_FAILURE; }
    
    if (!blast.loadFromFile(resourcePath() + "fire.png")) {
        return EXIT_FAILURE; }
    
    if (!fork.loadFromFile(resourcePath() + "fork.png")) {
        return EXIT_FAILURE; }
    
    sf::IntRect size(0,0,128,128);
    bull.setSize(sf::Vector2f (128,128));
    bull.setTexture(&def);
    bull.setScale(0.8, 0.8);
   // bull.setFillColor(sf::Color::Cyan);
    

}

void bullet::fire(){
    bull.move(0, -10);
}

void bullet::draw(sf::RenderWindow& window) {
    window.draw(bull);
    
}

void bullet::setpos(int x,int y) {
    bull.setPosition(x, y);
}

void bullet::handle_events(sf::Event event, bool& fire) {
    switch(event.key.code) {
        case sf::Keyboard::Space:
            fire=true;
            break;
    }
}

int bullet::getLeft() {
    return bull.getPosition().x;
    
}

int bullet::getTop() {
    return bull.getPosition().y;

}

int bullet::getRight() {
    return bull.getPosition().x + bull.getSize().x;
}

int bullet::getBottom() {
    return bull.getPosition().y + bull.getSize().y;
    
}


bool bullet::isFire(bool& fire){
    return fire;
}


void bullet::changeWeapon() {
    bull.setSize(sf::Vector2f(140,135));
    bull.setTexture(&beam);
    bull.setScale(0.5, 0.5);
    
}

