//
//  player2.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/24/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "player2.hpp"
#include <iostream>
using namespace std;
player2::player2() {
    
    if (!spaceship2.loadFromFile(resourcePath() + "space2.png")) {
        cout<< "error";
        return EXIT_FAILURE; }
    
    
    if (!explosion2.loadFromFile(resourcePath() + "exp2.png")) {
        cout<< "error";
        return EXIT_FAILURE; }

    
    ss2.setTexture(&spaceship2);
    ss2.setSize(sf::Vector2f(640,640));
    
    sf::IntRect size(0,0,640,640);
    sf::IntRect expsize(0,0,266,266);
    
    exp.setSize(sf::Vector2f(266,266));
    exp.setTexture(&explosion2);
    exp.setTextureRect(expsize);
    
    ss2.setScale(0.4, 0.4);
    ss2.setPosition(1000, 1300);
    
    
}

void player2::move(int x,int y) {
    ss2.move(x,y);
}
void player2::draw(sf::RenderWindow& window) {
    window.draw(ss2);
    //window.display();
}

void player2::setrect(sf::IntRect size) {
    ss2.setTextureRect(size);
}

void player2::handle_events(sf::Event event) {
    switch(event.key.code) {
            
        case sf::Keyboard::D:
            ss2.move(50,0);
            break;
        case sf::Keyboard::A:
            ss2.move(-50,0);
            break;
        case sf::Keyboard::W:
            ss2.move(0,-50);
            break;
        case sf::Keyboard::S:
            ss2.move(0,50);
            break;
    }
    
}

int player2::getx() {
    return ss2.getPosition().x;
    
}

int player2::gety() {
    return ss2.getPosition().y;
    
}

int player2::getLeft() {
    return ss2.getPosition().x;
    
}

int player2::getTop() {
    return ss2.getPosition().y;
    
}

int player2::getRight() {
    return ss2.getPosition().x + ss2.getSize().x;
}

int player2::getBottom() {
    return ss2.getPosition().y + ss2.getSize().y;
    
}

void player2::destroy(sf::RenderWindow& window) {
    ss2.setPosition(238742, 328477);
    //window.draw(exp);
    
    
}

void player2::checkLives() {
    
    ss2.setPosition(1000, 1300);
    
    //cout<<lives;
    lives--;
    
    if(lives==0) {
        ss2.setPosition(838382, 3728279);
        
    }
    
    
}

bool player2::checkDead() {
    bool flag=false;
    if(lives==0) flag=true;
    return flag;
}


