//
//  Chicken.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Chicken.hpp"
#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>


sf::Clock c;

Chicken::Chicken()
{
    chicken.setSize(sf::Vector2f(188, 163));
    sf::Texture chickentexture;
    chickentexture.loadFromFile(resourcePath() + "chicken.png");
    chicken.setTexture(&chickentexture);
    sf::Vector2u texturesize = chickentexture.getSize();
    texturesize.x /= 5;
    texturesize.y /= 1;
}


void Chicken:: settexturerect(int n)
{
    chicken.setTextureRect(sf::IntRect(texturesize.x * n, texturesize.y * 0, texturesize.x, texturesize.y));
    
}

void Chicken::setpos(sf::Vector2f newPos) {
    chicken.setPosition(newPos);
}

void Chicken:: checkcoll(bullet bullet)
{
    if ((bullet.getRight() > chicken.getPosition().x) && (bullet.getTop() < chicken.getPosition().y + chicken.getSize().y)
        && (bullet.getBottom() > chicken.getPosition().y) )
        chicken.setPosition(sf::Vector2f(423333846, 476292736) );
}

void Chicken::draw(sf::RenderWindow&window)
{
    window.draw(chicken);
}

int Chicken::getX() {
    return chicken.getPosition().x;
    
}

int Chicken::getY() {
    return chicken.getPosition().y;
    
}
