//
//  player1.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/16/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef player1_hpp
#define player1_hpp
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <stdio.h>
#include "ResourcePath.hpp"
#include "rocks.hpp"


class player1{
private:
    sf::Texture spaceship;
    sf::Texture explosion;
    bool dead=false;
    
    

public:
    sf::RectangleShape ss;
    sf::RectangleShape exp;
    player1();
    int lives=3;
    void move(int x, int y);
    void draw(sf::RenderWindow& window);
    void setrect(sf::IntRect size);
    void handle_events(sf::Event event);
    int getx();
    int gety();
    int getLeft();
    int getTop();
    int getBottom();
    int getRight();
    void destroy(sf::RenderWindow& window);
    void checkLives();
    bool checkDead();
    //void checkPickupColl(pickups gift);

            
};

    
#endif /* player1_hpp */
