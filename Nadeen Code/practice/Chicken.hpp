//
//  Chicken.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Chicken_hpp
#define Chicken_hpp

#include <SFML/Graphics.hpp>
#pragma once
#include <string>
#include <stdio.h>
#include "bullet.hpp"
#include "ResourcePath.hpp"





class Chicken
{
private:
    sf::RectangleShape chicken;
    sf::Texture chickentexture;
    sf::Vector2u texturesize;
    
public:
    Chicken ();
    void settexturerect(int n);
    void setpos(sf::Vector2f newPos);
    void checkcoll(bullet bullet);
    void draw (sf::RenderWindow&window);
    int getX();
    int getY();
    
};

#endif



