//
//  Beast1.hpp
//  practice
//
//  Created by Nadeen Mohamed on 4/29/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Beast1_hpp
#define Beast1_hpp
#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include "bullet.hpp"
#include "ResourcePath.hpp"





class Beast1
{
private:
    sf::RectangleShape beast;
    sf::Texture beasttexture;
    sf::Vector2u texturesize;
    
public:
    Beast1();
    void setexplosion();
    void settexturerect(int n);
    void setpos(float x, float y);
    void draw(sf::RenderWindow&window);
    void movebeast(int speed);
    int getX();
    int getY();
    bool checkcoll(bullet bullet);
    
};

#endif


