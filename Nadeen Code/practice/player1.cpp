//
//  player1.cpp
//  practice
//
//  Created by Nadeen Mohamed on 4/16/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "player1.hpp"
#include <iostream>
using namespace std;

player1::player1() {
   
    if (!spaceship.loadFromFile(resourcePath() + "space.png")) {
        cout<< "error";
        return EXIT_FAILURE; }
    
    if (!explosion.loadFromFile(resourcePath() + "exp1.png")) {
        cout<< "error";
        return EXIT_FAILURE; }
    
    ss.setTexture(&spaceship);
    ss.setSize(sf::Vector2f(640,640));

    sf::IntRect size(0,0,640,640);
    sf::IntRect expsize(0,0,266,266);
    
    exp.setSize(sf::Vector2f(266,266));
    exp.setTexture(&explosion);
    exp.setTextureRect(expsize);
    
    ss.setScale(0.4, 0.4);
    ss.setPosition(1200, 1300);
    
    
    
    }

void player1::move(int x,int y) {
    ss.move(x,y);
}
void player1::draw(sf::RenderWindow& window) {
    //if(dead==false)
    window.draw(ss);
}

void player1::setrect(sf::IntRect size) {
    ss.setTextureRect(size);
}

void player1::handle_events(sf::Event event) {
    switch(event.key.code) {
            
        case sf::Keyboard::Right:
            ss.move(50,0);
            break;
        case sf::Keyboard::Left:
            ss.move(-50,0);
            break;
        case sf::Keyboard::Up:
            ss.move(0,-50);
            break;
        case sf::Keyboard::Down:
            ss.move(0,50);
            break;
    }
    
}

int player1::getx() {
    return ss.getPosition().x;
    
}

int player1::gety() {
    return ss.getPosition().y;
    
}


int player1::getLeft() {
    return ss.getPosition().x;
    
}

int player1::getTop() {
    return ss.getPosition().y;
    
}

int player1::getRight() {
    return ss.getPosition().x + ss.getSize().x;
}

int player1::getBottom() {
    return ss.getPosition().y + ss.getSize().y;
    
}

void player1::destroy(sf::RenderWindow& window) {
    ss.setPosition(238742, 328477);
    //window.draw(exp);

    
}

void player1::checkLives() {
    
    ss.setPosition(1200, 1300);

    //cout<<lives;
    lives--;
 
    if(lives==0) {
        ss.setPosition(838382, 3728279);
        
    }
    
    
    }

bool player1::checkDead() {
    bool flag=false;
    if(lives==0) flag=true;
    return flag;
}
/*
void player1::checkCollision(rocks r) {
    
    if(ss.getPosition().y<=r.getBottom()) cout<< "collision" <<endl;
}
 */

//void player1::checkPickupColl(pickups gift){
    
//}
