//
//  Playersno.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/8/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Playersno_hpp
#define Playersno_hpp

#include <stdio.h>
#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <cstdlib>
#include <string>
#include <random>
#include <time.h>
#include "ResourcePath.hpp"



using namespace sf;
using namespace std;

class Playersno {
public:
    void initialize(Color, Color, Color, Color);
    void render(RenderWindow &Window);
    void handle(RenderWindow &Window, Vector2i &location, int &tap, Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue, /*bool &flagMouse,*/ Event event, bool&flag);
    
private:
    
    int CellSize;
    RectangleShape OnePlayer, TwoPlayers;
    Text OnePlayerText, TwoPlayersText;
    Font font, font2;
    Texture texture;
    Sprite sprite;
};

#endif /* Playersno_hpp */
