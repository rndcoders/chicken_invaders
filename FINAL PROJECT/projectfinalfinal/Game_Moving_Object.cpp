//
//  Game_Moving_Object.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Game_Moving_Object.hpp"

void Game_Moving_Object::setMovingPattern(const sf::Vector2f& P)
{
    moving_pattern = P;
}
void Game_Moving_Object::setMovingPattern(float x, float y)
{
    moving_pattern.x=x;
    moving_pattern.y=y;
}

const sf::Vector2f& Game_Moving_Object:: getMovingPattern() const
{
    return moving_pattern;
}

Game_Moving_Object::~Game_Moving_Object(){
    
}
