//
//  Egg.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Egg_hpp
#define Egg_hpp


#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "Small_Moving_Objects.hpp"

class Egg: public Small_Moving_Objects
{
  
public:
    Egg();
    ~Egg();
    
};
#endif /* Egg_hpp */
