//
//  Player.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Player.hpp"

Player::Player()
{
}

Player::~Player()
{
}

void Player::setLives(int l) {
    lives=l;
}

int Player::getLives() const {
    return lives;
    
}

void Player::incrementLives() {
    lives++;
}

void Player::decrementLives() {
    lives--;
}

void Player::setInitialPosition(const sf::Vector2f& pos){
    initial=pos;
    
}
const sf::Vector2f& Player::getIntialPosition(){
    return initial;
}

void Player::setBulletTexture(sf::Texture* t){
    bullet_texture=t;
}

sf::Texture* Player::getBulletTexture(){
    return bullet_texture;
}

void Player::setBulletScale(float x, float y){
    sx=&x;
    sy=&y;
    
}
const sf::Vector2f& Player::GetBulletScale(){
   return Game_Moving_Sprite::S.getScale();
    
    //return sx;
}
float*  Player::GetBulletScaley(){
    return sy;
    
}
