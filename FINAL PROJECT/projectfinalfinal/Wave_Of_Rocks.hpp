//
//  Wave_Of_Rocks.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/5/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Wave_Of_Rocks_hpp
#define Wave_Of_Rocks_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include "Player.hpp"
#include "Wave.hpp"
#include "Rock.hpp"

class Wave_Of_Rocks: public Wave {
   
private:
    
protected:
    sf::Texture* rock_texture;
    std::vector <Rock> rocks;
    int noRocks;
    void drawWave(sf::RenderWindow& window, std::vector<Player>& players);
    sf::IntRect* rock_size;
    
    
    sf::Vector2f rock_base_position;
    
    int rocks_probability;

    sf::Font* font;
    sf::Text text;
    
    void createNewRock(sf::RenderWindow& window );
    void eraseRocks(sf::RenderWindow& window);

public:
    
    
    Wave_Of_Rocks();
    Wave_Of_Rocks(std::vector<Player>& p, sf::Texture* rock_t);
    ~Wave_Of_Rocks();
    void setTexture(sf::Texture* t);
    bool playWave(sf::RenderWindow& window, std::vector<Player>& players, int& new_score);
    
    void setBasePosition(sf::Vector2f b);
    
    void createRock(Rock& rock, sf::RenderWindow& window );

    void moveRocks(sf::RenderWindow& window);
    
    void collisionRockBullet();
    void collisionRockPlayer(std::vector<Player>& players);
    
    void setFont(sf::Font* f);
    
    
    const sf::Vector2f& getBasePosition();
    
    void setNumberRocks(int n);
    
    bool winWave();
    
    void setWinText();
    
    void setTextureRect(sf::IntRect* r);
    
    void setProbability(int p);
    
    void killRock();
    
    sf::Vector2f RandomPosition(sf::RenderWindow& window);

    
};

#endif /* Wave_Of_Rocks_hpp */
