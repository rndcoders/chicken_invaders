//
//  Wave_Of_Beast.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/6/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Wave_Of_Beast.hpp"
#include <iostream> 

bool Wave_Of_Beast::playWave(sf::RenderWindow& window, std::vector<Player>& players, int strength, int& new_score)
{
    
    createBar();

    score=new_score;
    createBeast(b,strength);
    bullets.resize(players.size());
    death_immunity.resize(players.size());
    tens_immunity.resize(players.size());

    
    sf::Event event;
    int player_speed = 10;
    
    while (window.isOpen() && b.getStrength() > 0 && players.size() > 0) {
        setscoretext();

        
        movebeast(window);
        dropEggs();
        moveEggs();
        destroyEggs(window);
        
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        
        movePlayers(players, player_speed, window);
        FirePlayers(players);
        moveBullets();
        eraseBullets();
        
        collisionBeastPlayer(players);
        collisionPlayerEgg(players);
        
        collisionBeastBullet(window);
        
        
        for(int i = 0; i < players.size(); i++) {
            updateImmunityClock(i, players);
        }
        
        window.clear();
        drawWave(window, players);
        
        
        window.display();
    }
    
    new_score=score;
    if(!window.isOpen())
        return false;
    
    if(players.size() > 0)
        return true;
    
    return false;
}
Wave_Of_Beast::Wave_Of_Beast(std::vector<Player>& p, sf::Texture* beast_t, sf::Texture* egg_t, sf::IntRect *I, sf::Texture* bar)
{
    beast_texture = beast_t;
    egg_texture = egg_t;
   // score = 0;
    eggs.resize(0);
    HB_texture=bar;
    beast_size = I;
    
}

Wave_Of_Beast::Wave_Of_Beast()
{
    //score = 0;
    eggs.resize(0);
    
}


Wave_Of_Beast::~Wave_Of_Beast()
{
    
}

void Wave_Of_Beast::setBeastTexture(sf::Texture* t) {
    beast_texture = t;
}
void Wave_Of_Beast::setEggTexture(sf::Texture* t) {
    egg_texture = t;
}

void Wave_Of_Beast::drawWave(sf::RenderWindow& window, std::vector<Player>& players) {
    
    window.draw(back);
    
    
    drawhealthbar(window);
    
    window.draw(scoretext);

    
    
    b.draw(window);
    
    for (int i = 0; i< players.size(); i++) {
        players[i].draw(window);
        for (int j = 0; j < bullets[i].size(); j++)
            bullets[i][j].draw(window);
    }
    
    for (int i = 0; i<eggs.size(); i++)
        
        eggs[i].draw(window);
    
}

void Wave_Of_Beast::createBeast(Beast& b, int &strength) {
    b.setTexture(beast_texture);
    b.setTextureRect(*beast_size);
    b.setStrength(strength);
    b.setScale(3,3);
    b.setMovingPattern(2,2);
    b.setPosition(beast_base_position);
}

void Wave_Of_Beast::moveEggs() {
    for (int i = 0; i<eggs.size(); i++)
        
        eggs[i].move();
}

void Wave_Of_Beast::setBasePosition(sf::Vector2f b) {
    beast_base_position = b;
}

const sf::Vector2f& Wave_Of_Beast::getBasePosition() {
    return beast_base_position;
}

int Wave_Of_Beast::getProbability() {
    return probability;
    
}
void Wave_Of_Beast::setEggProbability(int p) {
    probability = p;
}

void Wave_Of_Beast::dropEggs(){
    sf::Vector2f pos;
    sf::Vector2f move_p;
    
    if (rand() % probability == 0){
        Egg newegg;
        switch(rand() % 5){
            case 0:
                move_p.x = 0; move_p.y = 10;
                pos.x = b.getPosition().x + b.getSize().x / 2 -110;
                pos.y =  b.getPosition().y + b.getSize().y - 90;
                break;
            case 1:
                move_p.x = 10; move_p.y = 0;
                pos.x = b.getPosition().x + b.getSize().x - 90;
                pos.y = b.getPosition().y + b.getSize().y / 2;
                break;
            case 2:
                move_p.x = -10; move_p. y= 0;
                pos.x = b.getPosition().x + 50;
                pos.y = b.getPosition().y + b.getSize().y / 2 - 60;
                break;
            case 3:
                move_p.x = 10; move_p. y= 10;
                pos.x = b.getPosition().x + b.getSize().x - 190;
                pos.y = b.getPosition().y + b.getSize().y -150;
                break;
            case 4:
                move_p.x = -10; move_p. y= 10;
                pos.x = b.getPosition().x + 80;
                pos.y = b.getPosition().y + b.getSize().y -160;
                break;
        }
        createEgg(newegg, pos, move_p);
        eggs.push_back(newegg);
        lay.setBuffer(*egg_lay);
        lay.play();
    }
}

void Wave_Of_Beast::destroyEggs(sf::RenderWindow& window) {
    sf::FloatRect X(0, 0, window.getSize().x, window.getSize().y);
    for (int k = 0; k<eggs.size(); k++)
        if(!eggs[k].getGlobalBounds().intersects(X)){
            eggs.erase(eggs.begin() + k);
            k--;
        }
}

void Wave_Of_Beast::createEgg(Egg& egg, const sf::Vector2f& pos, const sf::Vector2f& move_p) {
    egg.setTexture(egg_texture);
    egg.setTextureRect(sf::IntRect(0, 0, 47, 34));
    egg.setPosition(pos);
    egg.setMovingPattern(move_p);
    egg.setScale(2, 2);
    egg.setStrength(1);
}


void Wave_Of_Beast::flipBeasty()
{
    b.setMovingPattern(b.getMovingPattern().x , 0 - b.getMovingPattern().y );
}

void Wave_Of_Beast::flipBeastx()
{
    int l, t, w, h;
    l = beast_size->left;
    t = beast_size->top;
    w = beast_size->width;
    h = beast_size->height;
    
    b.setMovingPattern( 0 - b.getMovingPattern().x, b.getMovingPattern().y);
    
    if(b.getMovingPattern().x > 0)
         beast_size->left = 0;
    else
        beast_size->left = beast_size->width;
    
    b.setTextureRect(*beast_size);
    
    
        
    
}

void Wave_Of_Beast::movebeast(sf::RenderWindow& window) {
    if(b.getMovingPattern().y < 0 && b.getPosition().y - 5 < 0)
        flipBeasty();
    if(b.getMovingPattern().y > 0 && b.getPosition().y + b.getSize().y + 5 > window.getSize().y)
        flipBeasty();
    if(b.getMovingPattern().x < 0 && b.getPosition().x - 5 < 0)
        flipBeastx();
    if(b.getMovingPattern().x > 0 && b.getPosition().x + b.getSize().x + 5 > window.getSize().x)
        flipBeastx();

    b.move();
}

void Wave_Of_Beast::collisionBeastBullet(sf::RenderWindow& window) {
    for (int k = 0; k < bullets.size(); k++)
        for (int m = 0; m < bullets[k].size(); m++)
            if (isColliding(b, bullets[k][m])) {
                b.decStrength(bullets[k][m].getStrength());
                bullets[k].erase(bullets[k].begin() + m);
                m--;
                
                score+=50;
                
                if (b.getStrength() > 0 && b.getStrength() <=20)
                {
                    bar.setTextureRect(sf::IntRect(816, 0, 204, 30));
                    std ::  cout << b.getStrength() << std :: endl;

                }
                
                if (b.getStrength() >= 20 && b.getStrength() < 40) {
                    bar.setTextureRect(sf::IntRect(612, 0, 204, 30));
                    std ::  cout << b.getStrength() << std :: endl;

                }

                
                if (b.getStrength() >= 40 && b.getStrength() < 60)
                {
                    bar.setTextureRect(sf::IntRect(408, 0, 204, 30));
                    std ::  cout << b.getStrength() << std :: endl;

                }

                if (b.getStrength() >= 60 && b.getStrength() < 80)
                {
                    
                    bar.setTextureRect(sf::IntRect(204, 0, 204, 30));
                    std ::  cout << b.getStrength() << std :: endl;

                }

                
                if (b.getStrength() >= 80 && b.getStrength() < 100)
                {
                    std ::  cout << b.getStrength() << std :: endl;
                    
                    bar.setTextureRect(sf::IntRect(0, 0, 204, 30));
                }
                
                
                
                if (b.getStrength() == 0)
                {
                    bar.setTextureRect(sf::IntRect(1020, 0, 204, 30));
                }
                //std::cout << "shot" << std::endl;
                
            }    
}



void Wave_Of_Beast::collisionPlayerEgg(std::vector<Player>& players) {
    bool egg_erased;
    for (int n = 0; n < eggs.size(); n++) {
        egg_erased = false;
         for (int i = 0; i < players.size() && !egg_erased; i++) {
            if (isColliding(eggs[n], players[i]) && death_immunity[i].getElapsedTime().asSeconds() > 1)
            {
                killPlayer(players, i);
                eggs.erase(eggs.begin() + n);
                n--;
                egg_erased = true;
            }
        }
    }
}



void Wave_Of_Beast::collisionBeastPlayer(std::vector<Player>& players) {
    for (int i = 0; i < players.size(); i++) {
        if (isColliding(b, players[i]) && death_immunity[i].getElapsedTime().asSeconds() > 1)
            killPlayer(players, i);
    }
}


bool Wave_Of_Beast::winWave() {
    if (b.getStrength() == 0) return true;
    else return false;
}

void Wave_Of_Beast::setFont(sf::Font* f) {
    font = f;
}

void Wave_Of_Beast::setWinText() {
    text.setString("YOU WIN!");
    text.setCharacterSize(100);
    text.setOrigin(0, 0);
    text.setPosition(750, 450);
    text.setFillColor(sf::Color::White);
    text.setFont(*font);
    
}







void Wave_Of_Beast:: setEggBuffer(sf::SoundBuffer* e){
    egg_lay=e;
}

void Wave_Of_Beast:: setHitBuffer(sf::SoundBuffer* h){
    hitbuff=h;
}












void Wave_Of_Beast::drawhealthbar(sf:: RenderWindow & window)
{
    window.draw(bar);
}


void Wave_Of_Beast::createBar(){
    bar.setTexture(*HB_texture);
    bar.setTextureRect(sf::IntRect(0,0,204,30));
    bar.setPosition(800, 0);
    bar.setScale(3, 3);
    
}

