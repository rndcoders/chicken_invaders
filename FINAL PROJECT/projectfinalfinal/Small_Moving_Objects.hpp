//
//  Small_Moving_Objects.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Small_Moving_Objects_hpp
#define Small_Moving_Objects_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include "Game_Moving_Sprite.hpp"

class Small_Moving_Objects: public Game_Moving_Sprite {
protected:
    int strength;
public:
    void setStrength(int s);
    int getStrength();
    
};

#endif /* Small_Moving_Objects_hpp */
