//
//  Wave.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Wave_hpp
#define Wave_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>

#include "Player.hpp"
#include "Bullet.hpp"
#include "Pickups.hpp"
#include <vector>

class Wave{
protected:
    int score;
    
    int Highscore;
    
    //sf::Texture* bullet_texture;
    sf::Texture* background_texture;
    
    std::vector<sf::Clock> death_immunity;
    std::vector <sf::Clock> tens_immunity;
    
    
    int strength=10;
    float scale_x= 0.3;
    float scale_y=0.3;
    
    
    Bullet new_b;
    
    int pickup_probability;
    
    
    
    
    sf::Sprite back;
    sf::Sprite p;
    
    std::vector<std::vector<Bullet>> bullets;
        void moveBullets();
    void eraseBullets();
    void Fire(Player& p, int i);
    void FirePlayers(std::vector<Player>& players);
    sf::Clock bullets_clock[2];
    
    static const sf::Keyboard::Key Controls[10];
    void movePlayers(std::vector<Player>&players, int speed, sf::RenderWindow& window);
    void movePlayer(Player&, sf::Keyboard::Key Up,  sf::Keyboard::Key Down,  sf::Keyboard::Key right,  sf::Keyboard::Key left, int speed, sf::RenderWindow& window);
    
    void killPlayer(std::vector<Player>& players,int i);
    
    void drawPickups(sf::RenderWindow& window);
    void movePickups();
    void dropPickups(const sf::Vector2f& pos);
    void collisionPlayerPickup(std::vector<Player>& players);
    
    void updateImmunityClock(int i, std::vector<Player>& players);

    void setPickupProbability(int p);


    
    void destroyPickups(sf::RenderWindow& window);
    
    
    sf::SoundBuffer* buff;
    sf::SoundBuffer* bewbew;
    sf::SoundBuffer* woosh;
    sf::SoundBuffer* boop;
    sf::Sound shoot;
    
    sf::SoundBuffer* boombuff;
    sf::Sound boom;
    
    sf::Clock player_clock;
    
    
    sf::Texture* powerup;
    sf::Texture* wep1;
    sf::Texture* wep2;
    sf::Texture* wep3;

    std::vector <Pickups> pickups;

    
    
    sf::Texture* fork;
    sf::Texture* fire;
    sf::Texture* beam;
    sf::Texture* def;
    
    sf::Text scoretext;
    sf::Text highscoretext;
    sf::Font* font;
    

    
    
public:
    void setBulletsTextures(sf::Texture* t1, sf::Texture* t2, sf::Texture* t3, sf::Texture* t4, std::vector<Player>&players);
    
    
    int getScore();
    void setScore(int s);
     void setscoretext();
    
    
    void setBackground(sf::Texture* t);
    void setSoundBuffer(sf::SoundBuffer* b);
     void setBoomBuffer(sf::SoundBuffer* b);
    void Animation(std::vector<Player>& players);
    
    //void setSoundBuffer(sf::SoundBuffer* pew);
    //sf::SoundBuffer getSoundBuffer();

    //pickups
    
    void loadPickupsTextures(sf::Texture* p, sf::Texture* w1, sf::Texture* w2, sf::Texture* w3);
    void loadWeaponsSounds(sf::SoundBuffer* bew, sf::SoundBuffer* wo, sf::SoundBuffer* bo);
    
    void changeWeapon(Player& p,int t);
    void incBullSize(Player& p);
    //void changeBullet
    
    
    void setFontTexture(sf::Font* f);
        
   };


#endif /* Wave_hpp */
