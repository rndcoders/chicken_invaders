//
//  Beast.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/6/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Beast_hpp
#define Beast_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "Enemy.hpp"


class Beast : public Enemy {
private:
protected:
 
    
    
    
public:
    Beast();
    Beast(sf::Texture *healthbar);
   
    virtual ~Beast();
    
};


#endif /* Beast_hpp */
