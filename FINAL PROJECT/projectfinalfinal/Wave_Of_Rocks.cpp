//
//  Wave_Of_Rocks.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/5/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Wave_Of_Rocks.hpp"

#include <iostream>


Wave_Of_Rocks::Wave_Of_Rocks(std::vector<Player>& p, sf::Texture* rock_t){
    rock_texture=rock_t;
    //score=0;
    rocks.resize(0);
    noRocks=0;
    
    
}

Wave_Of_Rocks::Wave_Of_Rocks(){
    //score=0;
    rocks.resize(0);
    noRocks=0;
}

Wave_Of_Rocks::~Wave_Of_Rocks(){
    
}

void Wave_Of_Rocks::drawWave(sf::RenderWindow& window, std::vector<Player>& players){
    
    window.draw(back);
    window.draw(scoretext);

    
    for(int i=0; i< players.size(); i++) {
            players[i].draw(window);
        for(int j =0; j < bullets[i].size(); j++)
            bullets[i][j].draw(window);
    }
    
    for(int i=0; i<rocks.size(); i++) {
        rocks[i].draw(window);
    }
}

void Wave_Of_Rocks::setTexture(sf::Texture* t){
    rock_texture=t;
    
}
bool Wave_Of_Rocks::playWave(sf::RenderWindow& window, std::vector<Player>& players, int& new_score){
    score= new_score;
    
    bullets.resize(players.size());
    death_immunity.resize(players.size());
    tens_immunity.resize(players.size());

    
    sf::Event event;
    int player_speed=10;
    
    while(window.isOpen() && players.size() > 0 && (noRocks > 0  || rocks.size() > 0)) {
        setscoretext();

        
        bool flag = false;
        createNewRock(window);
        
        moveRocks(window);
        eraseRocks(window);
        
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        
        movePlayers(players, player_speed, window);
        FirePlayers(players);
        moveBullets();
        eraseBullets();
        
        collisionRockBullet();
        collisionRockPlayer(players);
        
        
        for(int i = 0; i < players.size(); i++) {
            updateImmunityClock(i, players);
        }
    
        window.clear();
        drawWave(window, players);
        
        window.display();
    }
    
    new_score=score;
    
    if(!window.isOpen())
        return false;
    
    if(players.size()>0)
        return true;
    else
        return false;
}

void Wave_Of_Rocks::setBasePosition(sf::Vector2f b){
    rock_base_position=b;
    
}

sf::Vector2f Wave_Of_Rocks::RandomPosition(sf::RenderWindow& window)
{
    if(rand() % 2 == 0)
        return sf::Vector2f(0, rand() % int(window.getSize().y - 150));
    else
        return sf::Vector2f(rand() % int(window.getSize().x - 150), 0);
}

void Wave_Of_Rocks::createRock(Rock& rock, sf::RenderWindow& window){
    rock.setTexture(rock_texture);
    rock.setPosition(RandomPosition(window));
    rock.setTextureRect(*rock_size);
    rock.setStrength(50);
    rock.setMovingPattern(5, 5);
    
    float x = (rand() % 6) / 10.0 + 0.5;
    
    rock.setScale(x, x);
}

void Wave_Of_Rocks::createNewRock(sf::RenderWindow& window){
    if(noRocks > 0 && rand() % rocks_probability == 0){
        Rock rockie;
        createRock(rockie,window);
        rocks.push_back(rockie);
        noRocks--;
    }
}

void Wave_Of_Rocks::moveRocks(sf::RenderWindow& window){
    for(int i=0; i < rocks.size(); i++)
        rocks[i].move();
}

void Wave_Of_Rocks::eraseRocks(sf::RenderWindow& window){
    sf::FloatRect X(0, 0, window.getSize().x, window.getSize().y);
    for (int k = 0; k<rocks.size(); k++)
        if(!rocks[k].getGlobalBounds().intersects(X)){
            rocks.erase(rocks.begin() + k);
            k--;
        }

}

void Wave_Of_Rocks::collisionRockBullet(){
    bool rock_destroyed;
    for(int i = 0; i < rocks.size(); i++){
        rock_destroyed = false;
        for(int k = 0; k < bullets.size() && !rock_destroyed; k++)
            for(int m = 0; m < bullets[k].size() && !rock_destroyed; m++)
                if(isColliding(rocks[i], bullets[k][m])){
                    rocks[i].decStrength(bullets[k][m].getStrength());
                    bullets[k].erase(bullets[k].begin() + m);
                    m--;
                    score+=75;
                    if(rocks[i].getStrength() < 1){
                        rocks.erase(rocks.begin() + i);
                        i--;
                        rock_destroyed = true;
                    }
                }
    }
}
void Wave_Of_Rocks::collisionRockPlayer(std::vector<Player>& players){
    
    for(int i = 0; i < players.size(); i++){
        for(int k = 0; k < rocks.size(); k++)
            if(isColliding(rocks[k], players[i]) && death_immunity[i].getElapsedTime().asSeconds() > 1){
                killPlayer(players, i);
                rocks.erase(rocks.begin() + k);
                k--;
                }
    }
}

void Wave_Of_Rocks::setFont(sf::Font* f){
    font=f;
}


const sf::Vector2f& Wave_Of_Rocks::getBasePosition(){
    return rock_base_position;
    
}

void Wave_Of_Rocks::setNumberRocks(const int n){
    noRocks = n;
}

bool Wave_Of_Rocks::winWave(){
    if(noRocks < 1)
    return true;
    else return false;
        
}

void Wave_Of_Rocks::setWinText(){
    
    text.setString("YOU WIN!");
    text.setCharacterSize(100);
    text.setOrigin(0, 0);
    text.setPosition(750, 450);
    text.setFillColor(sf::Color::White);
    text.setFont(*font);
    

    
}


void Wave_Of_Rocks:: setTextureRect(sf::IntRect* r){
    rock_size=r;
}


void Wave_Of_Rocks::setProbability(int p) {
    rocks_probability=p;
}

