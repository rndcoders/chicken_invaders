//
//  Bullet.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Bullet_hpp
#define Bullet_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>



#include "Small_Moving_Objects.hpp"

class Bullet: public Small_Moving_Objects{
private:
    
public:
    Bullet();
    ~Bullet();
    
};

#endif /* Bullet_hpp */
