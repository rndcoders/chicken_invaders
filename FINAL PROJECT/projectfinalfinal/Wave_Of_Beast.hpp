//
//  Wave_Of_Beast.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/6/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Wave_Of_Beast_hpp
#define Wave_Of_Beast_hpp

#include <stdio.h>
#include "Wave.hpp"
#include "Player.hpp"
#include "Egg.hpp"
#include "Beast.hpp"
#include <vector>




class Wave_Of_Beast : public Wave {
    protected :
    Beast b;
    
    sf::Sprite bar;
    
    //sf::IntRect* bar_size;

private:
    sf::Texture *HB_texture;
    std::vector <Egg>eggs;
    void drawWave(sf::RenderWindow& window, std::vector<Player>& players);
    sf::Texture* beast_texture;
    sf::Texture* egg_texture;
    sf::IntRect*bar_size;

    
    
    
    sf::IntRect* beast_size;
    
    int rows;
    int cols;
    sf::Vector2f beast_base_position;
    int probability;
    
    
    
    sf::Font* font;
    sf::Text text;
    
    sf::SoundBuffer* egg_lay;
    sf::Sound lay;
    
    sf::SoundBuffer* hitbuff;
    sf::Sound hit;

    
public:
    int shot_chickens = 0;
    
    Wave_Of_Beast();
    Wave_Of_Beast(std::vector<Player>& p, sf::Texture* beast_t, sf::Texture* egg_t, sf::IntRect *I, sf::Texture* bar);
    void setBeastTexture(sf::Texture* t);
    void setEggTexture(sf::Texture* t);
    ~Wave_Of_Beast();
    bool playWave(sf::RenderWindow& window, std::vector<Player>& players, int strength, int& new_score);
    bool win();
    //int getNumEnemies();
    void moveEggs();
    void setBasePosition(sf::Vector2f b);
    
    void createBeast(Beast &b, int &strenth);
    
    void flipBeasty();
    void flipBeastx();
    void movebeast(sf::RenderWindow& window);
    
    int getProbability();
    void setEggProbability(int p);
    
    void dropEggs();
    void destroyEggs(sf::RenderWindow& window);
    
    void createEgg(Egg& egg, const sf::Vector2f& pos, const sf::Vector2f& move_p);
    
    void createBar();
    
    

    
    
    void collisionBeastBullet(sf::RenderWindow& window);
    void collisionPlayerEgg(std::vector<Player>& players);
    void collisionBeastPlayer(std::vector<Player>& players);
    
    void setFont(sf::Font* f);
    
    
    const sf::Vector2f& getBasePosition();
    
    
    bool winWave();
    
    void setWinText();
    
    void setBarTexture(sf::Texture* b);
    void drawhealthbar(sf:: RenderWindow & window);
    
    void setEggBuffer(sf::SoundBuffer* e);
    
    void setHitBuffer(sf::SoundBuffer* h);
  
    
    
};

#endif /* Wave_Of_Beast_hpp */
