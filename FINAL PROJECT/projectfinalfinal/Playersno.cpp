#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include<cstdlib>
#include<string>
#include <random>
#include <time.h>
#include "Playersno.hpp"
using namespace sf;
using namespace std;


void Playersno::initialize(Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue)
{

	if (!texture.loadFromFile(resourcePath() + "cover.jpg"))
	{
		cout << "error cover photo" << endl;
	}


	sprite.setTexture(texture);
	sprite.setScale(1.5, 1.5);

	OnePlayer.setSize(Vector2f(300, 100));
	OnePlayer.setPosition(100, 350);
	OnePlayer.setOutlineThickness(4);
	OnePlayer.setOutlineColor(Blue2);
	OnePlayer.setFillColor(LightBlue);



	TwoPlayers.setSize(Vector2f(300, 100));
	TwoPlayers.setPosition(450, 350);
	TwoPlayers.setOutlineThickness(4);
	TwoPlayers.setOutlineColor(Blue2);
	TwoPlayers.setFillColor(LightBlue);

	//text

	//if (!font.loadFromFile(resourcePath() + "fontt.ttf")) {
		//cout << "font error " << endl;
	//}
	if (!font2.loadFromFile(resourcePath() + "sansation.ttf")) {
		cout << "sansation font error " << endl;
	}


	OnePlayerText.setFont(font2);
	OnePlayerText.setString("1 Player");
	OnePlayerText.setCharacterSize(60);
	OnePlayerText.setFillColor(Blue2);
	OnePlayerText.move(150, 360);


	TwoPlayersText.setFont(font2);
	TwoPlayersText.setString("2 Players");
	TwoPlayersText.setCharacterSize(60);
	TwoPlayersText.setFillColor(Blue2);
	TwoPlayersText.move(500, 360);
}

void Playersno::handle(RenderWindow &Window, Vector2i &location, int &tap, Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue, /*bool &flagMouse,*/ Event event, bool&flag) // Music &music)
	{
		
		
	/*	location = Mouse::getPosition(Window);

		if (event.type == Event::MouseButtonReleased)
			flagMouse = false;

		if (flagMouse == false)
		{
		*/

			if (OnePlayer.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
			{
				tap = 6;
				flag = true;

			}

			if (TwoPlayers.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
			{
				tap = 7;
				flag = true;

			}
		}
	
	
		void Playersno::render(RenderWindow &Window)
		{

			Window.draw(sprite);
			Window.draw(OnePlayer);
			Window.draw(TwoPlayers);
			Window.draw(OnePlayerText);
			Window.draw(TwoPlayersText);
			
		}
