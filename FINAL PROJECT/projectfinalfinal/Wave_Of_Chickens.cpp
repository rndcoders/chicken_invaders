//
//  Wave_Of_Chickens.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Wave_Of_Chickens.hpp"

#include <iostream>


Wave_Of_Chickens::Wave_Of_Chickens(std::vector<Player>& p, sf::Texture* chicken_t, sf::Texture* egg_t)
{
    chicken_texture=chicken_t;
    egg_texture=egg_t;
    score = 30;
    chickens.resize(0);
    eggs.resize(0);
    chicken_size=sf::IntRect(0,0,188,163);
    rows=0;
    cols=0;
    pickups.resize(0);
    
}

Wave_Of_Chickens::Wave_Of_Chickens()
{
    score=30;
    chickens.resize(0);
    eggs.resize(0);
    chicken_size=sf::IntRect(0,0,188,163);
    rows=0;
    cols=0;
    
    pickups.resize(0);

}

Wave_Of_Chickens::~Wave_Of_Chickens()
{
    
}


void Wave_Of_Chickens::setChickenTexture(sf::Texture* t){
    chicken_texture=t;
}
void Wave_Of_Chickens::setEggTexture(sf::Texture* t){
    egg_texture=t;
}

void Wave_Of_Chickens::drawWave(sf::RenderWindow& window, std::vector<Player>& players){
    
    window.draw(back);

    window.draw(scoretext);
    
    for(int i=0; i<chickens.size(); i++)
        for(int j=0; j<chickens[i].size(); j++)
            chickens[i][j].draw(window);
    
    for(int i=0; i< players.size(); i++){
        players[i].draw(window);
        for(int j =0; j < bullets[i].size(); j++)
            bullets[i][j].draw(window);
    }
    
    for(int i=0; i<eggs.size(); i++)
        for(int j=0; j<eggs[i].size(); j++)
            for(int k=0; k<eggs[i][j].size(); k++)
                eggs[i][j][k].draw(window);
    
    drawPickups(window);
}

bool Wave_Of_Chickens::playWave(sf::RenderWindow& window, std::vector<Player>& players, int& new_score){
    score = new_score;
    
    createChickens();
    bullets.resize(players.size());
    death_immunity.resize(players.size());
    tens_immunity.resize(players.size());
    
    sf::Event event;
    int player_speed=10;
    
     sf::Clock clock_text;
    if(winWave()==true) {
        clock_text.restart();
        if(clock_text.getElapsedTime().asSeconds() < 100) {
            std::cout<< std::fixed << clock_text.getElapsedTime().asSeconds();
            setWinText();
            window.draw(text);
            
        }
    }

    while(window.isOpen() && chickens.size() > 0 && players.size() > 0){
        setscoretext();
        
       //music->play();
        
        setMusic();
        // music.play();
        
        
        Animation(players); 
        moveChickens(window);
        dropEggs();
        moveEggs();
        destroyEggs(window);
        
        destroyPickups(window);
        movePickups();
        collisionPlayerPickup(players);

        while (window.pollEvent(event)){
            if (event.type == sf::Event::Closed)
                window.close();
        }
        
        movePlayers(players, player_speed, window);
        FirePlayers(players);
        moveBullets();
        eraseBullets();
        
        collisionChickPlayer(players);
        collisionChickBullet();
        collisionPlayerEgg(players);
    
        for(int i = 0; i < players.size(); i++) {
            updateImmunityClock(i, players);
        }
        
        window.clear();
        drawWave(window, players);
        
        
        window.display();
    }
    
    new_score = score;
    
    //std::cout << "3" << std::endl;

    
    if(!window.isOpen())
        return false;
    
    if(players.size()>0) return true;
    else return false;
}

bool Wave_Of_Chickens::win(){
}

void Wave_Of_Chickens::createChickens(){
    Chicken chick;
    createChicken(chick);
    chick.setMovingPattern(chick_moving_pattern);
    eggs.resize(rows);
    for(int i=0; i<rows; i++) {
        eggs[i].resize(cols);
        std::vector<Chicken> f;
        for(int j=0; j<cols; j++) {
            eggs[i][j].resize(0);
            chick.setPosition(sf::Vector2f(chick_base_position.x + (chick.getSize().x + 20) * j, chick_base_position.y + (chick.getSize().y + 20) * i));
            f.push_back(chick);
        }
        chickens.push_back(f);
    }
    
}

void Wave_Of_Chickens::createChicken(Chicken& chick){
    chick.setTexture(chicken_texture);
    chick.setTextureRect(chicken_size);
    chick.setStrength(10);
    chick.setScale(0.8, 0.8);
}

void Wave_Of_Chickens::moveEggs(){
    for(int i=0; i<eggs.size();i++)
        for(int j=0; j<eggs[i].size(); j++)
            for(int k=0; k<eggs[i][j].size(); k++)
                eggs[i][j][k].move();
}

void Wave_Of_Chickens::setRows(int r) {
    rows=r;
}

void Wave_Of_Chickens::setCols(int c) {
    cols=c;
}

int Wave_Of_Chickens::getNumEnemies(){
    int lol;
    return lol;
}

void Wave_Of_Chickens::setBasePosition(sf::Vector2f& b){
    chick_base_position=b;
}

const sf::Vector2f& Wave_Of_Chickens::getBasePosition(){
    return chick_base_position;
}

int Wave_Of_Chickens::getProbability(){
    return probability;
    
}
void Wave_Of_Chickens::setEggProbability(int p){
    probability=p;
}

void Wave_Of_Chickens::dropEggs(){
    for(int i=0; i<chickens.size(); i++)
        for(int j=0; j<chickens[i].size(); j++) {
            if(rand()%probability==0) {
                Egg newegg;
                createEgg(newegg, chickens[i][j]);
                eggs[i][j].push_back(newegg);
                lay.setBuffer(*egg_lay);
                lay.play();
            }
        }
}

void Wave_Of_Chickens::destroyEggs(sf::RenderWindow& window) {
    for(int i=0; i<chickens.size(); i++)
        for(int j=0; j<chickens[i].size(); j++)
            for(int k=0; k<eggs[i][j].size(); k++) {
                if(eggs[i][j][k].getPosition().y>window.getSize().y) {
                    eggs[i][j].erase(eggs[i][j].begin() + k);
                    k--;
                }
            }
}

void Wave_Of_Chickens::createEgg(Egg& egg, Chicken& chick){
    egg.setTexture(egg_texture);
    egg.setTextureRect(sf::IntRect(0,0,47,34));
    egg.setPosition(sf::Vector2f(chick.getPosition().x + chick.getSize().x / 2, chick.getPosition().y + chick.getSize().y + 5));
    egg.setMovingPattern(0,5);
    egg.setScale(2, 2);
    egg.setStrength(1);
}

void Wave_Of_Chickens::flipChickensRow(std::vector<Chicken>& chick){
    for(int i = 0; i < chick.size(); i++)
        chick[i].setMovingPattern(sf::Vector2f(0, 0) - chick[i].getMovingPattern());
}

void Wave_Of_Chickens::moveChickens(sf::RenderWindow& window){
    for(int i = 0; i < chickens.size(); i++){
        if(chickens[i][0].getPosition().x < 10 && chickens[i][0].getMovingPattern().x < 0)
            flipChickensRow(chickens[i]);
        if(chickens[i][chickens[i].size()-1].getPosition().x + chickens[i][chickens[i].size()-1].getSize().x > window.getSize().x - 10 && chickens[i][chickens[i].size()-1].getMovingPattern().x > 0)
            flipChickensRow(chickens[i]);
        for(int j = 0; j < chickens[i].size(); j++)
            chickens[i][j].move();
    }
}

void Wave_Of_Chickens::collisionChickBullet(){
    bool chick_killed;
    for(int i = 0; i < chickens.size(); i++){
        for(int j = 0; j < chickens[i].size(); j++){
            chick_killed = false;
            for(int k = 0; k < bullets.size() && chick_killed==false; k++)
                for(int m = 0; m < bullets[k].size() && chick_killed==false; m++)
                    if(isColliding(chickens[i][j], bullets[k][m])){
                        
                        score += 50;
                        
                        if(rand()%10==0) dropPickups(chickens[i][j].getPosition());
                        
                        bullets[k].erase(bullets[k].begin() + m);
                        m--;
                        chickens[i].erase(chickens[i].begin() + j);
                        j--;
                        chick_killed = true;
                        hit.setBuffer(*chick_hit);
                        hit.setVolume(1000);
                        hit.play();
                    }
        }
        if(chickens[i].size() == 0)
            chickens.erase(chickens.begin() + i--);
    }
}

void Wave_Of_Chickens::collisionPlayerEgg(std::vector<Player>& players ){
    for(int i = 0; i < players.size(); i++)
        for(int k = 0; k < eggs.size(); k++)
            for(int m = 0; m < eggs[k].size(); m++)
                for(int n=0; n<eggs[k][m].size(); n++)
                 if(isColliding(eggs[k][m][n], players[i]) && death_immunity[i].getElapsedTime().asSeconds() > 1){
                     killPlayer(players, i);
                     if(!players[i].immunity){
                        eggs[k][m].erase(eggs[k][m].begin() + n);
                        n--;
                     }
            }
}

void Wave_Of_Chickens::collisionChickPlayer(std::vector<Player>& players) {
    for(int i = 0; i < players.size(); i++)
        for(int k = 0; k < chickens.size(); k++)
            for(int m = 0; m < chickens[k].size(); m++)
                if(isColliding(chickens[k][m], players[i]) && death_immunity[i].getElapsedTime().asSeconds() > 1){
                    killPlayer(players, i);
                    if(!players[i].immunity){
                        chickens[k].erase(chickens[k].begin() + m);
                        m--;
                    }
                }
    
}

bool Wave_Of_Chickens::winWave() {
    bool all_dead;
    for(int i=0; i<chickens.size(); i++) {
        for(int j=0; j<chickens[i].size(); j++)
            if(chickens[i].size()>0){
                //std::cout<<"lol"<<std::endl;
                all_dead=false;
            }
            else {
                all_dead=true;
            }
    }
    
    return all_dead;
}

                 

void Wave_Of_Chickens::setWinText(){
    text.setString("Advancing to next wave..");
    text.setCharacterSize(100);
    text.setOrigin(0, 0);
    text.setPosition(430, 450);
    text.setFillColor(sf::Color::White);
    text.setFont(*font);

}


void Wave_Of_Chickens::setMusic(){
    if (!music.openFromFile(resourcePath() + "chicken_level.wav")) {
        return EXIT_FAILURE;
    }
        
}

void Wave_Of_Chickens::setChickBuffer(sf::SoundBuffer* b){
    chick_hit=b;
}

void Wave_Of_Chickens::setEggBuffer(sf::SoundBuffer* e){
    egg_lay=e;
    
}



void Wave_Of_Chickens::setChickMovingPattern(sf::Vector2f m){
    chick_moving_pattern=m;
}
const sf::Vector2f&  Wave_Of_Chickens::getChickMovingPattern(){
    return chick_moving_pattern;
    
}

