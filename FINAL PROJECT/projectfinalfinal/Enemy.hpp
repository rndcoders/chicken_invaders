//
//  Enemy.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Enemy_hpp
#define Enemy_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "Game_Moving_Sprite.hpp"

class Enemy: public Game_Moving_Sprite{
protected:
    int strength;
    int numEnemies;
    
public:
    void setStrength(int s);
    int getStrength();
    void decStrength(int s);
    void incStrength(int s);
    
    
};


#endif /* Enemy_hpp */
