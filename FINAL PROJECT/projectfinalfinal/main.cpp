
//
// Disclaimer:
// ----------
//
// This code will work only if you selected window, graphics and audio.
//
// Note that the "Run Script" build phase will copy the required frameworks
// or dylibs to your application bundle so you can execute it on any OS X
// computer.
//
// Your resource files (images, sounds, fonts, ...) are also copied to your
// application bundle. To get the path to these resources, use the helper
// function `resourcePath()` from ResourcePath.hpp
//

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <algorithm>
#include <functional>

// Here is a small helper for you! Have a look.
#include "ResourcePath.hpp"

#include "Player.hpp"
#include "Bullet.hpp"
#include "Rock.hpp"
#include "Wave_Of_Chickens.hpp"
#include "Wave_Of_Rocks.hpp"
#include "Wave_Of_Beast.hpp"
#include "SetStartMenu.hpp"
#include "SetSettingsMenu.hpp"
#include "Playersno.hpp"

using namespace sf;
using namespace std;


//Global Values




sf::IntRect player_size(0,0,640,640);
sf::IntRect beast1_size(0,0,168,160);
sf::IntRect beast2_size(0,0,227,243);
sf::IntRect beast3_size(0,0,227,243);
sf::IntRect rock_size(0,0,240,200);


int player_speed = 40;
float bull_dis = -10;


sf::Font font;
sf::Texture background_texture;
vector<Player> players;

sf::Sound trans;
sf::SoundBuffer transbuff;

void createPlayer(Player& player, sf::Texture* t, const sf::Vector2f& pos);
void createBullet(Bullet& bull, Player& player, sf::Texture* t, float dis);
void deleteExtraBullets(std::vector<Bullet>& bullets);
void drawBullets(std::vector<Bullet>& bullets, sf::RenderWindow& window);
void moveBullets(std::vector<Bullet>& bullets, sf::RenderWindow& window);
void delay(sf::Clock& delay, sf::RenderWindow& window);
void win(sf::Clock& delay, sf::RenderWindow& window);
void delayp(sf::Clock& delay, sf::RenderWindow& window);
void printText(string s, sf::RenderWindow& window);
void savehighscore(int n);
void displayScores(sf::RenderWindow& window);

SetStartMenu SetstartMenuObj ;
SetSettingsMenu SetSettingsMenuObj ;
Playersno PlayersnoObj;


Color DarkBlue = { 0, 0, 53 }, Blue2{ 65, 71, 69 }, LightBlue{ 235, 208, 181 }, SeaBlue{ 153, 255, 255 };


int new_score = 0;

int main(int, char const**)
{
    // Create the main window
    sf::RenderWindow window(sf::VideoMode(1900, 1300), "Chicken Invaders");
    
    sf::Image icon;
    if (!icon.loadFromFile(resourcePath() + "icon_texture.png")) {
        return EXIT_FAILURE;
    }
    window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

    
    
    sf::Clock player_clock;
    
    srand(unsigned(time(NULL)));
    
    //Loading Textures and Fonts
   
    if(!background_texture.loadFromFile(resourcePath() + "background.jpg"))
        return EXIT_FAILURE;
    
    sf::Texture player1_texture;
    if(!player1_texture.loadFromFile(resourcePath() + "space.png"))
        return EXIT_FAILURE;
    
    sf::Texture player2_texture;
    if(!player2_texture.loadFromFile(resourcePath() + "space2.png"))
        return EXIT_FAILURE;
    
    sf::Texture bullet_texture;
    if(!bullet_texture.loadFromFile(resourcePath() + "def_bull.png"))
        return EXIT_FAILURE;
    
    sf::Texture bullet_texture2;
    if(!bullet_texture2.loadFromFile(resourcePath() + "bullet2.png"))
        return EXIT_FAILURE;
    
    sf::Texture chicken_texture;
    if(!chicken_texture.loadFromFile(resourcePath() + "chick.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture chicken_texture2;
    if(!chicken_texture2.loadFromFile(resourcePath() + "chick2.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }

    sf::Texture chicken_texture3;
    if(!chicken_texture3.loadFromFile(resourcePath() + "chick3.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }

    
    sf::Texture egg_texture;
    if(!egg_texture.loadFromFile(resourcePath() + "eggsprite.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture rock_texture;
    
    sf::Texture rock_texture1;
    if(!rock_texture1.loadFromFile(resourcePath() + "rock1sprite.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }

    sf::Texture rock_texture2;
    if(!rock_texture2.loadFromFile(resourcePath() + "rock2sprite.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }

    sf::Texture rock_texture3;
    if(!rock_texture3.loadFromFile(resourcePath() + "rock3sprite.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture beast1;
    if(!beast1.loadFromFile(resourcePath() + "beast1sprite.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture beast2;
    if(!beast2.loadFromFile(resourcePath() + "beast2.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture beast3;
    if(!beast3.loadFromFile(resourcePath() + "beats 3.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture health_bar;
    if(!health_bar.loadFromFile(resourcePath() + "barsprite.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture powerup_texture;
    if(!powerup_texture.loadFromFile(resourcePath() + "powerup.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture pickup1_texture;
    if(!pickup1_texture.loadFromFile(resourcePath() + "pickup1.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture pickup2_texture;
    if(!pickup2_texture.loadFromFile(resourcePath() + "pickup2.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture pickup3_texture;
    if(!pickup3_texture.loadFromFile(resourcePath() + "pickup3.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture fork_texture;
    if(!fork_texture.loadFromFile(resourcePath() + "fork.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture fire_texture;
    if(!fire_texture.loadFromFile(resourcePath() + "fire.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::Texture beam_texture;
    if(!beam_texture.loadFromFile(resourcePath() + "beam.png")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }

    
    if(!font.loadFromFile(resourcePath() + "sansation.ttf")) {
        std::cout<<"error";
        return EXIT_FAILURE;
    }
    
    sf::SoundBuffer pewbuff;
    if (!pewbuff.loadFromFile(resourcePath() + "pew.wav")) {
        return EXIT_FAILURE;
    }
    
    sf::SoundBuffer chickdiebuff;
    if (!chickdiebuff.loadFromFile(resourcePath() + "chicken_hit.wav")) {
        return EXIT_FAILURE;
    }
    
    sf::SoundBuffer egglaybuff;
    if (!egglaybuff.loadFromFile(resourcePath() + "egg_lay.wav")) {
        return EXIT_FAILURE;
    }
    
    sf::SoundBuffer boombuff;
    if (!boombuff.loadFromFile(resourcePath() + "player_hit.wav")) {
        return EXIT_FAILURE;
    }

    sf::SoundBuffer bosshitbuff;
    if (!bosshitbuff.loadFromFile(resourcePath() + "boss_hit.wav")) {
        return EXIT_FAILURE;
    }
    
    sf::SoundBuffer bossdeadbuff;
    if (!bossdeadbuff.loadFromFile(resourcePath() + "player_hit.wav")) {
        return EXIT_FAILURE;
    }

    sf::Music chicken_level;
    if (!chicken_level.openFromFile(resourcePath() + "chicken_level.wav")) {
        //return EXIT_FAILURE;
    }

    
    if (!transbuff.loadFromFile(resourcePath() + "transition.wav")) {
        //return EXIT_FAILURE;
    }
    
    //pew.play();

    
    //Declaring Menu
    
    
    SetstartMenuObj.initialize(Blue2, LightBlue, DarkBlue, SeaBlue);
    SetSettingsMenuObj.initialize(Blue2, LightBlue, DarkBlue, SeaBlue);
    PlayersnoObj.initialize(Blue2, LightBlue, DarkBlue, SeaBlue);
    
    Music music;
    if ( !music.openFromFile(resourcePath()+ "chicken_menu.wav") )
        cout << "okay" << endl;
    music.play();

    
    //Declaring Player 1
    
    Player player;
    createPlayer(player, &player1_texture, sf::Vector2f(950, 1200));
    /*
    //Declaring Bullet
    std::vector<Bullet> bullets;
    Bullet bull;

    */
    
    sf::Vector2f pos=sf::Vector2f(100,100);

    //Declaring a chicken
    
    //Declaring a rock
    Rock rockie;
    
    int x=rand()%3;
    std::cout<< x << std::endl;
    switch(x) {
        case 0: rock_texture=rock_texture1; break;
        case 1: rock_texture=rock_texture2; break;
        case 2: rock_texture=rock_texture3; break;
    }
  
    
    
    
    
    
    
    
    Player player1;
    createPlayer(player1, &player1_texture, sf::Vector2f(950, 1000));
    Player player2;
    createPlayer(player2, &player2_texture, sf::Vector2f(600, 1000));
    players.push_back(player1);
    //players.push_back(player2);

    
    
    Wave_Of_Chickens wave1(players,&chicken_texture,&egg_texture);
    wave1.setBackground(&background_texture);
    wave1.setScore(0);
    wave1.setChickenTexture(&chicken_texture);
    wave1.setEggTexture(&egg_texture);
    wave1.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave1.setChickMovingPattern(sf::Vector2f(4,0));
    wave1.setSoundBuffer(&pewbuff);
    wave1.setChickBuffer(&chickdiebuff);
    wave1.setEggBuffer(&egglaybuff);
    wave1.setBoomBuffer(&boombuff);
    wave1.setFontTexture(&font);
    wave1.setBasePosition(pos);
    wave1.setCols(5);
    wave1.setRows(5);
    wave1.setEggProbability(5000);
    wave1.loadPickupsTextures(&powerup_texture, &pickup1_texture, &pickup2_texture, &pickup3_texture);
    
    Wave_Of_Chickens wave4(players,&chicken_texture,&egg_texture);
    wave4.setBackground(&background_texture);
    wave4.setChickenTexture(&chicken_texture);
    wave4.setEggTexture(&egg_texture);
    wave4.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave4.setChickMovingPattern(sf::Vector2f(3,3));
    wave4.setSoundBuffer(&pewbuff);
    wave4.setChickBuffer(&chickdiebuff);
    wave4.setEggBuffer(&egglaybuff);
    wave4.setBoomBuffer(&boombuff);
    wave4.setFontTexture(&font);
    wave4.setBasePosition(pos);
    wave4.setCols(6);
    wave4.setRows(3);
    wave4.setEggProbability(3500);
    wave4.loadPickupsTextures(&powerup_texture, &pickup1_texture, &pickup2_texture, &pickup3_texture);
    
    
    Wave_Of_Chickens wave5(players,&chicken_texture2,&egg_texture);
    wave5.setBackground(&background_texture);
    wave5.setScore(0);
    wave5.setChickenTexture(&chicken_texture2);
    wave5.setEggTexture(&egg_texture);
    wave5.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave5.setChickMovingPattern(sf::Vector2f(4,0));
    wave5.setSoundBuffer(&pewbuff);
    wave5.setChickBuffer(&chickdiebuff);
    wave5.setEggBuffer(&egglaybuff);
    wave5.setBoomBuffer(&boombuff);
    wave5.setFontTexture(&font);
    wave5.setBasePosition(pos);
    wave5.setCols(8);
    wave5.setRows(5);
    wave5.setEggProbability(2000);
    wave5.loadPickupsTextures(&powerup_texture, &pickup1_texture, &pickup2_texture, &pickup3_texture);
    
    Wave_Of_Chickens wave6(players,&chicken_texture2,&egg_texture);
    wave6.setBackground(&background_texture);
    wave6.setScore(0);
    wave6.setChickenTexture(&chicken_texture2);
    wave6.setEggTexture(&egg_texture);
    wave6.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave6.setChickMovingPattern(sf::Vector2f(3,3));
    wave6.setSoundBuffer(&pewbuff);
    wave6.setChickBuffer(&chickdiebuff);
    wave6.setEggBuffer(&egglaybuff);
    wave6.setBoomBuffer(&boombuff);
    wave6.setFontTexture(&font);
    wave6.setBasePosition(pos);
    wave6.setCols(7);
    wave6.setRows(3);
    wave6.setEggProbability(2000);
    wave6.loadPickupsTextures(&powerup_texture, &pickup1_texture, &pickup2_texture, &pickup3_texture);
    
    Wave_Of_Chickens wave9(players,&chicken_texture3,&egg_texture);
    wave9.setBackground(&background_texture);
    wave9.setScore(0);
    wave9.setChickenTexture(&chicken_texture3);
    wave9.setEggTexture(&egg_texture);
    wave9.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave9.setChickMovingPattern(sf::Vector2f(4,0));
    wave9.setSoundBuffer(&pewbuff);
    wave9.setChickBuffer(&chickdiebuff);
    wave9.setEggBuffer(&egglaybuff);
    wave9.setBoomBuffer(&boombuff);
    wave9.setFontTexture(&font);
    wave9.setBasePosition(pos);
    wave9.setCols(10);
    wave9.setRows(6);
    wave9.setEggProbability(1500);
    wave9.loadPickupsTextures(&powerup_texture, &pickup1_texture, &pickup2_texture, &pickup3_texture);
    
    Wave_Of_Chickens wave10(players,&chicken_texture3,&egg_texture);
    wave10.setBackground(&background_texture);
    wave10.setScore(0);
    wave10.setChickenTexture(&chicken_texture3);
    wave10.setEggTexture(&egg_texture);
    wave10.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave10.setChickMovingPattern(sf::Vector2f(3,3));
    wave10.setSoundBuffer(&pewbuff);
    wave10.setChickBuffer(&chickdiebuff);
    wave10.setEggBuffer(&egglaybuff);
    wave10.setBoomBuffer(&boombuff);
    wave10.setFontTexture(&font);
    wave10.setBasePosition(pos);
    wave10.setCols(8);
    wave10.setRows(4);
    wave10.setEggProbability(1500);
    wave10.loadPickupsTextures(&powerup_texture, &pickup1_texture, &pickup2_texture, &pickup3_texture);

    

    
    window.setFramerateLimit(100);
    
    //wave1.playWave(window, players);
    
    
    Wave_Of_Rocks wave2(players,&rock_texture1);
    wave2.setBackground(&background_texture);
    int number=30  ;
    wave2.setNumberRocks(number);
    wave2.setBasePosition(sf::Vector2f(0,0));
    wave2.setTextureRect(&rock_size);
    wave2.setFontTexture(&font);
    wave2.setSoundBuffer(&pewbuff);
    wave2.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave2.setProbability(50);
    wave2.setBoomBuffer(&boombuff);
    
    Wave_Of_Rocks wave7(players,&rock_texture2);
    wave7.setBackground(&background_texture);
    int number2=45 ;
    wave7.setNumberRocks(number2);
    wave7.setBasePosition(sf::Vector2f(0,0));
    wave7.setTextureRect(&rock_size);
    wave7.setFontTexture(&font);
    wave7.setSoundBuffer(&pewbuff);
    wave7.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave7.setProbability(50);
    wave7.setBoomBuffer(&boombuff);
    
    Wave_Of_Rocks wave11(players,&rock_texture3);
    wave11.setBackground(&background_texture);
    int number3=60  ;
    wave11.setNumberRocks(number3);
    wave11.setBasePosition(sf::Vector2f(0,0));
    wave11.setTextureRect(&rock_size);
    wave11.setFontTexture(&font);
    wave11.setSoundBuffer(&pewbuff);
    wave11.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave11.setProbability(50);
    wave11.setBoomBuffer(&boombuff);


    
    
    window.setFramerateLimit(100);
    /*
    if(wave2.playWave(window, players)){
        std::cout << "You win" << std::endl;
    }
    else{
        if(window.isOpen())
            std::cout << "You lose" << std::endl;
        else
            std::cout << "Window is closed" << std::endl;
    }
     */
    Wave_Of_Beast wave3(players, &beast1, &egg_texture, &beast1_size, &health_bar);
    wave3.setBackground(&background_texture);
    wave3.setBasePosition(sf::Vector2f(0,0));
    wave3.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave3.setEggTexture(&egg_texture);
    wave3.setBeastTexture(&beast1);
    wave3.setFontTexture(&font);
    wave3.setSoundBuffer(&pewbuff);
    wave3.setEggProbability(50);
    wave3.setEggBuffer(&egglaybuff);
    wave3.setBoomBuffer(&boombuff);
    wave3.setHitBuffer(&bosshitbuff);
    
    Wave_Of_Beast wave8(players, &beast2, &egg_texture, &beast2_size, &health_bar);
    wave8.setBackground(&background_texture);
    wave8.setBasePosition(sf::Vector2f(500,500));
    wave8.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave8.setEggTexture(&egg_texture);
    wave8.setBeastTexture(&beast2);
    wave8.setFontTexture(&font);
    wave8.setSoundBuffer(&pewbuff);
    wave8.setEggProbability(15);
    wave8.setEggBuffer(&egglaybuff);
    wave8.setBoomBuffer(&boombuff);
    wave8.setHitBuffer(&bosshitbuff);
    
    Wave_Of_Beast wave12(players, &beast3, &egg_texture, &beast3_size, &health_bar);
    wave12.setBackground(&background_texture);
    wave12.setBasePosition(sf::Vector2f(500,0));
    wave12.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
    wave12.setEggTexture(&egg_texture);
    wave12.setBeastTexture(&beast3);
    wave12.setFontTexture(&font);
    wave12.setSoundBuffer(&pewbuff);
    wave12.setEggProbability(5);
    wave12.setEggBuffer(&egglaybuff);
    wave12.setBoomBuffer(&boombuff);
    wave12.setHitBuffer(&bosshitbuff);

    
    /*
    if(wave3.playWave(window, players, 100)){
        std::cout << "You win" << std::endl;
    }
    else{
        if(window.isOpen())
            std::cout << "You lose" << std::endl;
        else
            std::cout << "Window is closed" << std::endl;
    }
    */
    
    
    int tap = 1;
    int wave_number=0;
    int wave_number2=0;
    bool flag = false;
    
    
    while (window.isOpen())
    {
        //mouse location
        Vector2i location = Mouse::getPosition(window);
        Event event;
        
        while (window.pollEvent(event))
        {
            
            // Close window: exit
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            // Escape pressed: exit
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }
        }
        
        
        if (tap == 1)
        {
            SetstartMenuObj.render(window);
            SetstartMenuObj.handle( window , location, tap, Blue2, LightBlue, DarkBlue, SeaBlue , flag );
        }
        
        
        
        if (tap == 2)
        {
            PlayersnoObj.render(window);
            PlayersnoObj.handle(window, location, tap, Blue2, LightBlue, DarkBlue, SeaBlue, event, flag);
            
        }
        
        
        
        if (tap == 4)
        {
            SetSettingsMenuObj.render ( window );
            SetSettingsMenuObj.handle ( window, location, tap, Blue2, LightBlue , DarkBlue, SeaBlue, flag, event, music);
        }
        
        if (tap == 5)
        {
            window.close();
        }
        
        
        if( tap==6) {
          
            //cout << "okay" << endl;
            
            
            // Start the game loop
                switch (wave_number) {
                    case 0:
                        music.stop();
                        chicken_level.play();
                        chicken_level.setVolume(20);
                        
                        
                        
                        if(wave1.playWave(window, players, new_score)){
                            std::cout << "2" << std::endl;
                            sf::Clock d;
                            delay(d,window);
                            wave_number=1;

                            std::cout << wave_number << std::endl;
                        }
                        else {
                            if(window.isOpen())
                                window.close();
                        }
                        
                        break;
                        
                    case 1: {
                        
                        if(wave2.playWave(window, players, new_score)){
                            sf::Clock d2;
                            delay(d2,window);
                            wave_number=2;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                    }
                        break;
                        
                    case 2:
                        if(wave4.playWave( window, players, new_score)){
                            wave_number=3;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                        
                        break;
                    case 3:
                        if(wave3.playWave( window, players,100, new_score)){
                            sf::Clock d2;
                            delayp(d2,window);
                            wave_number=4;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                        
                        break;
                        
                    case 4:
                        //music.stop();
                        chicken_level.play();
                        chicken_level.setVolume(20);
                        
                        
                        
                        if(wave5.playWave(window, players, new_score)){
                            std::cout << "2" << std::endl;
                            sf::Clock d;
                            delay(d,window);
                            wave_number=5;
                            
                            std::cout << wave_number << std::endl;
                        }
                        else {
                            if(window.isOpen())
                                window.close();
                        }
                        
                        break;
                        
                    case 5: {
                        
                        if(wave7.playWave(window, players, new_score)){
                            sf::Clock d2;
                            delay(d2,window);
                            wave_number=6;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                    }
                        break;

                    case 6:
                        if(wave6.playWave( window, players, new_score)){
                            sf::Clock d2;
                            delay(d2,window);
                            wave_number=7;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                        
                        break;
                        
                    case 7:
                        if(wave8.playWave( window, players,100, new_score)){
                            sf::Clock d2;
                            delayp(d2,window);
                            wave_number=8;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                        
                        break;
                        
                    case 8:
                        //music.stop();
                        chicken_level.play();
                        chicken_level.setVolume(20);
                        
                        
                        
                        if(wave9.playWave(window, players, new_score)){
                            std::cout << "2" << std::endl;
                            sf::Clock d;
                            delay(d,window);
                            wave_number=9;
                            
                            std::cout << wave_number << std::endl;
                        }
                        else {
                            if(window.isOpen())
                                window.close();
                        }
                        
                        break;
                        
                    case 9: {
                        
                        if(wave11.playWave(window, players, new_score)){
                            sf::Clock d2;
                            delay(d2,window);
                            wave_number=10;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                    }
                        break;
                        
                    case 10:
                        if(wave10.playWave( window, players, new_score)){
                            sf::Clock d2;
                            delay(d2,window);
                            wave_number=11;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                        
                        break;
                        
                    case 11:
                        if(wave12.playWave( window, players,100, new_score)){
                            sf::Clock d2;
                            win(d2,window);
                            wave_number=-1;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                        
                        break;

                        
                        
                    case -1: {
                        window.close();
                    }
                        
                }
        }
        
            if( tap==7) {
                
                //cout << "okay" << endl;
                
                players.push_back(player2);
                
                wave1.setBulletsTextures(&bullet_texture, &fork_texture , &fire_texture, &beam_texture, players);
                
                tap = 6;
                
                /*
                // Start the game loop
                switch (wave_number2) {
                    case 0:
                        music.stop();
                        chicken_level.play();
                        chicken_level.setVolume(20);
                        if(wave1.playWave(window, players)){
                            std::cout << "2" << std::endl;
                            wave_number2=1;
                            
                            std::cout << wave_number2 << std::endl;
                        }
                        else {
                            if(window.isOpen())
                                window.close();
                        }
                        
                        break;
                        
                    case 1:
                        
                        if(wave2.playWave(window, players)){
                            wave_number2=2;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                        
                        break;
                    case 2:
                        if(wave3.playWave( window, players,500)){
                            wave_number2=-1;
                            //std::cout << "You win" << std::endl;
                        }
                        else {
                            if(window.isOpen()) window.close();
                            
                        }
                        
                        break;
                        
                    case -1: window.close();
                        
                }
                 */

            }
            
         
        
            
            
        window.display();
        window.clear();
        
    
    }

    
    //savehighscore(new_score);


    return EXIT_SUCCESS;
}

void createPlayer(Player& player, sf::Texture* t, const sf::Vector2f& pos)
{
    player.setTexture(t);
    player.setInitialPosition(pos);
    player.setPosition(pos);
    player.setTextureRect(player_size);
    //player.setColor(sf::Color(255,255,255,100));
    player.setScale(0.4, 0.4);
    player.setLives(5);
    player.setBulletScale(0.3, 0.3);
}


void createBullet(Bullet& bull, Player& player, sf::Texture* t, float dis)
{
    bull.setPosition(sf::Vector2f(player.getPosition().x + player.getSize().x / 3.3, player.getPosition().y - 65));
    bull.setTexture(t);
    bull.setScale(0.8, 0.8);
    bull.setMovingPattern(0, dis);
}

void deleteExtraBullets(std::vector<Bullet>& bullets){
    for(int i=0; i<bullets.size(); i++) {
        if(bullets[i].getPosition().y<0)
        {
            bullets.erase(bullets.begin()+i);
            i--;
        }
    }
    
}
void drawBullets(std::vector<Bullet>& bullets, sf::RenderWindow& window){
    
    for(int i = 0; i< bullets.size(); i++) {
 
        bullets[i].draw(window);
    }
}

void moveBullets(std::vector<Bullet>& bullets, sf::RenderWindow& window)
{
    for(int i = 0; i< bullets.size(); i++) {
        bullets[i].move();

    }
}


void delay(sf::Clock& delay, sf::RenderWindow& window) {
    sf::Sprite back;
    back.setTexture(background_texture);
    back.setScale(1, 1.2);
    while(true) {
        window.clear();
        window.draw(back);
        trans.setBuffer(transbuff);
        trans.play();
        for(int i=0; i< players.size(); i++){
            players[i].draw(window);
            //players[i].move();
        }
        printText("Advancing to next wave..", window);
        window.display();
        if(delay.getElapsedTime().asSeconds()> 2) return;
        
    }
    
}

void win(sf::Clock& delay, sf::RenderWindow& window) {
    sf::Sprite back;
    back.setTexture(background_texture);
    back.setScale(1, 1.2);
    while(true) {
        window.clear();
        window.draw(back);
        trans.setBuffer(transbuff);
        trans.play();
        for(int i=0; i< players.size(); i++){
            players[i].draw(window);
            //players[i].move();
        }
        printText("YOU WIN!", window);
        window.display();
        if(delay.getElapsedTime().asSeconds()> 2) return;
        
    }
    
}


void delayp(sf::Clock& delay, sf::RenderWindow& window) {
    sf::Sprite back;
    back.setTexture(background_texture);
    back.setScale(1, 1.2);
    while(true) {
        window.clear();
        window.draw(back);
        trans.setBuffer(transbuff);
        trans.play();
        for(int i=0; i< players.size(); i++){
            players[i].draw(window);
            //players[i].move();
        }
        printText("Advancing to next planet..", window);
        window.display();
        if(delay.getElapsedTime().asSeconds()> 2) return;
        
    }
    
}

void printText(string s, sf::RenderWindow& window) {
    sf::Text text;
    text.setString(s);
    text.setCharacterSize(100);
    text.setOrigin(0, 0);
    text.setPosition(430, 450);
    text.setFillColor(sf::Color::White);
    text.setFont(font);
    window.draw(text);
}




void savehighscore(int n) // call this function when window closes and pass the wave(num).getScore();
{
    string x = to_string(n);
    
    string A[100000];
    
    A[0] = x;
    ifstream inp;
    ofstream outp;
    int i = 1;
    string filename= "scores.txt";
    inp.open(filename.c_str());
    if (!inp.fail())
    {
        
        while (!inp.eof())
        {
            getline(inp, A[i]);
            i++;
            
        }
    }
    
    else
        std::cout << "error" <<std::endl;
    
    
    inp.close();
    
  // sort(A, A + 100000, greater<int>());
    
    
    outp.open(filename.c_str(), ofstream::out | ofstream::trunc);
    
    
    for (int j = 0; j < 100000; j++)
    {
        outp << A[i];
        outp.put('\n');
        
    }
    
    outp.close();
}


void displayScores(sf:: RenderWindow &window)
{
    sf::Text scores[100000];
    int x;
    int y;
    
    
    string A[100000];
    ifstream inp;
    //ofstream outp;
    int i = 0;
    
    inp.open("scores.txt");
    if (!inp.fail())
    {
        while (!inp.eof())
        {
            getline(inp, A[i]);
            i++;
        }
    }
    
    
    inp.close();
    
    for (int j = 0; j < 5; j++)
    {
        
        scores[j].setString(A[j]);
        scores[j].setCharacterSize(100);
        scores[j].setOrigin(0, 0);
        scores[j].setPosition(x, y);
        scores[j].setFillColor(sf::Color::White);
        scores[j].setFont(font);
        window.draw(scores[j]);
        
        y += 200;
        
    }
    
    
    
}


