//
//  Game_Moving_Rectangle.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Game_Moving_Rectangle.hpp"


void Game_Moving_Rectangle::setTexture(sf::Texture* t){
    R.setTexture(t);
}

void Game_Moving_Rectangle::setTextureRect(const sf::IntRect& I){
    R.setTextureRect(I);
}

void Game_Moving_Rectangle::setPosition(const sf::Vector2f& P)
{
    R.setPosition(P);
}
const sf::Vector2f& Game_Moving_Rectangle::getPosition() const
{
    return R.getPosition();
}

void Game_Moving_Rectangle::move(const sf::Vector2f& D)
{
    R.move(D);
}

void Game_Moving_Rectangle::move(float x, float y)
{
    R.move(x,y);
}

void Game_Moving_Rectangle::move()
{
    R.move(moving_pattern);
}

const sf::Vector2f& Game_Moving_Rectangle::getSize() const
{
    return R.getSize();
}

void Game_Moving_Rectangle::draw(sf::RenderWindow& window) const
{
    window.draw(R);
}



void Game_Moving_Rectangle::setColor(const sf::Color& c)
{
    R.setFillColor(c);
}
const sf::Color& Game_Moving_Rectangle::getColor() const
{
    return R.getFillColor();
}

void Game_Moving_Rectangle::setScale(float x, float y)
{
    R.setScale(x, y);
    
}
void Game_Moving_Rectangle::setScale(const sf::Vector2f& s)
{
    R.setScale(s);
}

const sf::Vector2f& Game_Moving_Rectangle::getScale() const
{
    return R.getScale();
}


sf::FloatRect Game_Moving_Rectangle::getGlobalBounds() const
{
    R.getGlobalBounds();
}


Game_Moving_Rectangle::~Game_Moving_Rectangle(){
    
}



