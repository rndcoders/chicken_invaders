//
//  Rock.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/5/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Rock_hpp
#define Rock_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "Enemy.hpp"


class Rock: public Enemy{
protected:
    
public:
    Rock();
   ~Rock();
    
};

#endif /* Rock_hpp */
