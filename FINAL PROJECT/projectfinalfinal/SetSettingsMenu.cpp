//
//  SetSettingsMenu.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/6/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "SetSettingsMenu.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include<cstdlib>
#include<string>
#include <random>
#include <time.h>



using namespace sf;
using namespace std;


void SetSettingsMenu::initialize(Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue)
{
    
   if (!texture.loadFromFile(resourcePath() +  "Cover.jpg"))
   {
        
        cout << "error cover photo" << endl;
        
        
    }
    sprite.setTexture(texture);
    sprite.setScale(1.5, 1.5);
    
    
    SoundRect.setPosition(100, 350);
    SoundRect.setSize(Vector2f(600, 100));
    SoundRect.setOutlineThickness(4);
    SoundRect.setOutlineColor(Blue2);
    SoundRect.setFillColor(LightBlue);
    
    
    MusicRect.setSize(Vector2f(600, 100));
    MusicRect.setPosition(100, 500);
    MusicRect.setOutlineThickness(4);
    MusicRect.setOutlineColor(Blue2);
    MusicRect.setFillColor(LightBlue);
    
    
    SoundRectCheck.setSize(Vector2f(100, 100));
    SoundRectCheck.setPosition(800, 500);
    SoundRectCheck.setOutlineThickness(4);
    SoundRectCheck.setOutlineColor(Blue2);
    SoundRectCheck.setFillColor(LightBlue);
    
    
    MusicRectCheck.setSize(Vector2f(100, 100));
    MusicRectCheck.setPosition(800, 350);
    MusicRectCheck.setOutlineThickness(4);
    MusicRectCheck.setOutlineColor(Blue2);
    MusicRectCheck.setFillColor(LightBlue);
    
    
    BackRect.setSize(Vector2f(250, 100));
    BackRect.setPosition(900, 650);
    BackRect.setOutlineThickness(4);
    BackRect.setOutlineColor(Blue2);
    BackRect.setFillColor(LightBlue);
    
    //text
   //if (!font.loadFromFile(resourcePath() +  "fontt.ttf"))
  // {
        //cout << "font error " << endl;
    //}
    
    if (!font2.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "sansation font error " << endl;
    }
    
    //Headline text
    textHeadline.setFont(font);
    textHeadline.setString("chicken invaders");
    textHeadline.setCharacterSize(75);
    textHeadline.setFillColor(Blue2);
    textHeadline.move(290, 10);
    
    
    // Music text
    MusicText.setFont(font2);
    MusicText.setString("Music");
    MusicText.setCharacterSize(60);
    MusicText.setFillColor(Blue2);
    MusicText.move(280, 360);
    
    
    // Sound text
    SoundText.setFont(font2);
    SoundText.setString("Sound");
    SoundText.setCharacterSize(60);
    SoundText.setFillColor(Blue2);
    SoundText.move(280, 510);
    
    //Settings text
    
    SettingsText.setFont(font);
    SettingsText.setString("Settings");
    SettingsText.setCharacterSize(110);
    SettingsText.setFillColor(Blue2);
    SettingsText.move(120, 160);
    
    //back text
    
    BackText.setFont(font2);
    BackText.setString("Back");
    BackText.setCharacterSize(60);
    BackText.setFillColor(Blue2);
    BackText.move(955, 660);
    
}

void SetSettingsMenu::render(RenderWindow &Window)
{
    
    Window.draw(sprite);
    Window.draw(BackRect);
    Window.draw(MusicRect);
    Window.draw(SoundRect);
    Window.draw(SoundRectCheck);
    Window.draw(MusicRectCheck);
    Window.draw(textHeadline);
    Window.draw(MusicText);
    Window.draw(SoundText);
    Window.draw(BackText);
    Window.draw(SettingsText);
}



void SetSettingsMenu::handle(RenderWindow &Window, Vector2i &location, int &tap, Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue, bool &flagMouse, Event event, Music &music)
{
    location = Mouse::getPosition(Window);
    
    if (event.type == Event::MouseButtonReleased)
        flagMouse = false;
    
    if (flagMouse == false)
    {
        
        
        //music check change color
        /*	if (MusicRectCheck.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left) && MusicRectCheck.getFillColor() == LightBlue)
         {
         MusicRectCheck.setFillColor(Color::Red);
         music.pause();
         flagMouse = true;
         
         }
         else if (MusicRectCheck.getGlobalBounds().contains(location.x, location.y) && MusicRectCheck.getFillColor() == Color::Red && Mouse::isButtonPressed(Mouse::Left))
         {
         MusicRectCheck.setFillColor(LightBlue);
         music.play();
         flagMouse = true;
         }*/
    }
    
    //	Window.draw(backgroundS);
    
    
    //sound select change color
    
    
    if (SoundRectCheck.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
    {
        SoundRectCheck.setFillColor(Color::Red);
        
    }
    else if ((SoundRectCheck.getGlobalBounds().contains(location.x, location.y) && SoundRectCheck.getFillColor() != Color::Red))
    {
        SoundRectCheck.setFillColor(SeaBlue);
    }
    else if (SoundRectCheck.getFillColor() != Color::Red)
    {
        SoundRectCheck.setFillColor(LightBlue);
    }
    
    
    
    // back button change state 
    
    
    if (BackRect.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
    {
        tap = 1;
    }
    
    else if ((BackRect.getGlobalBounds().contains(location.x, location.y) && BackRect.getFillColor() != Color::Red))
    {
        BackRect.setFillColor(SeaBlue);
    }
    else if (BackRect.getFillColor() != Color::Red)
    {
        BackRect.setFillColor(LightBlue);
    }
    
    
}
