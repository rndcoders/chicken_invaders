 //
//  Wave.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Wave.hpp"
#include <iostream>
int Wave::getScore(){
    return score;
}
void Wave::setScore(int s){
    score=s;
    
    
}

void Wave::destroyPickups(sf::RenderWindow& window){
    
    sf::FloatRect X(0, 0, window.getSize().x, window.getSize().y);
    for (int k = 0; k<pickups.size(); k++)
        if(!pickups[k].getGlobalBounds().intersects(X)){
            pickups.erase(pickups.begin() + k);
            k--;
        }
}

void Wave::drawPickups(sf::RenderWindow& window){
    for(int i = 0; i < pickups.size(); i++)
        pickups[i].draw(window);
}

void Wave::movePickups(){
    for(int i=0; i<pickups.size(); i++) {
        pickups[i].move();        
    }
}

void Wave::dropPickups(const sf::Vector2f& pos){
    Pickups pick;
    int x=rand()%9;
    // 0 and 1 for extra life, 2 and 3 for extra power, 4 and 5 for 10s immunity, 6 for fork, 7 for fire, 8 for beam;
    
    pick.setType(x);
    
    //Textures
    
    switch(x) {
        case 0: case 1: case 2: case 3: case 4: case 5:
            pick.setTexture(powerup); pick.setScale(0.5, 0.5);
            break;
        case 6:
            pick.setTexture(wep1); pick.setScale(0.25, 0.25); break;
        case 7:
            pick.setTexture(wep2); pick.setScale(0.25, 0.25); break;
        case 8:
            pick.setTexture(wep3); pick.setScale(0.25, 0.25); break;
    }
    
    pick.setPosition(pos);
    pick.setMovingPattern(0, 5);
    pickups.push_back(pick);
}

void Wave::setPickupProbability(int p){
    pickup_probability=p;
}


void Wave::collisionPlayerPickup(std::vector<Player>& players){
    for(int i = 0; i < players.size(); i++)
        for(int j=0; j < pickups.size(); j++)
            if(isColliding(players[i], pickups[j])){
                switch(pickups[j].getType()){
                        // 0 and 1 for extra life, 2 and 3 for extra power, 4 and 5 for 10s immunity, 6 for fork, 7 for fire, 8 for beam;
                    case 0: case 1:
                        players[i].incrementLives();
                        break;
                    case 2: case 3:
                        incBullSize(players[i]);
                        break;
                    case 4: case 5:
                        players[i].immunity = true;
                        players[i].setColor(sf::Color(255,255,255,100));
                        tens_immunity[i].restart();
                        break;
                    case 6:
                        changeWeapon(players[i],6);
                        break;
                    case 7:
                        changeWeapon(players[i],7);
                       break;
                    case 8:
                        changeWeapon(players[i],8);
                        break;
                }
                pickups.erase(pickups.begin() + j);
                j--;
            }
}

void Wave::updateImmunityClock(int i, std::vector<Player>& players){
    
    if(tens_immunity[i].getElapsedTime().asSeconds() >= 10){
        std::cout << "in" <<std::endl;
        tens_immunity[i].restart();
        players[i].immunity = false;
        players[i].setColor(sf::Color(255, 255, 255, 255));
    }
}



const sf::Keyboard::Key Wave::Controls[10] = {sf::Keyboard::Key::Up, sf::Keyboard::Down, sf::Keyboard::Right, sf::Keyboard::Left, sf::Keyboard::Space,sf::Keyboard::Key::W, sf::Keyboard::Key::S, sf::Keyboard::Key::D, sf::Keyboard::Key::A, sf::Keyboard::LShift};

void Wave::movePlayer(Player& p, sf::Keyboard::Key Up,  sf::Keyboard::Key Down,  sf::Keyboard::Key Right,  sf::Keyboard::Key Left, int speed, sf::RenderWindow& window)
{
    if(sf::Keyboard::isKeyPressed(Right) && p.getPosition().x + p.getSize().x + 10 < window.getSize().x)
        p.move(speed,0);
    if(sf::Keyboard::isKeyPressed(Left) && p.getPosition().x -10 > 0)
        p.move(-speed,0);
    if(sf::Keyboard::isKeyPressed(Up) && p.getPosition().y - 10 > 0)
        p.move(0,-speed);
    if(sf::Keyboard::isKeyPressed(Down) && p.getPosition().y + p.getSize().y + 10 < window.getSize().y)
        p.move(0,speed);
}

void Wave::setBulletsTextures(sf::Texture* t1, sf::Texture* t2, sf::Texture* t3, sf::Texture* t4, std::vector<Player>& players){
    for(int i=0; i<players.size(); i++) {
    players[i].setBulletTexture(t1);
   
    }
    
    def=t1;
    fork=t2;
    fire=t3;
    beam=t4;
}
void Wave::Fire(Player& p, int i) {
    
    if(sf::Keyboard::isKeyPressed(Controls[i * 5 + 4]) && bullets_clock[i].getElapsedTime().asSeconds() > 0.15){
        shoot.setBuffer(*buff);
        shoot.setVolume(10);
        shoot.play();
        //Bullet new_b;
        new_b.setTexture(p.getBulletTexture());
        new_b.setColor(sf::Color(192,192,192,255));
        new_b.setScale(p.GetBulletScale());
        new_b.setPosition(sf::Vector2f(p.getPosition().x + p.getSize().x / 3.5 + 20, p.getPosition().y - 40));
        new_b.setStrength(strength);
        new_b.setMovingPattern(0, -9);
        
        if(i >= 0 && i < 2)
            bullets[i].push_back(new_b);
        bullets_clock[i].restart();
    }
}
void Wave::moveBullets(){
    for (int i=0; i<bullets.size(); i++)
        for (int j=0; j< bullets[i].size(); j++)
            bullets[i][j].move();
}

void Wave::eraseBullets(){
    for (int i=0; i<bullets.size(); i++)
        for (int j=0; j< bullets[i].size(); j++)
            if(bullets[i][j].getPosition().y < 0)
                bullets[i].erase(bullets[i].begin()+j);
}


void Wave::movePlayers(std::vector<Player>&players, int speed, sf::RenderWindow& window){
    for(int i = 0; i < players.size() && i < 2; i++)
        movePlayer(players[i], Wave::Controls[i * 5], Wave::Controls[i * 5 + 1], Wave::Controls[i * 5 + 2], Wave::Controls[i * 5 + 3], speed, window);
    
}

void Wave::FirePlayers(std::vector<Player>& players){
    for(int i = 0; i < players.size() && i < 2; i++)
        Fire(players[i], i);
    
    
}

void Wave::setBackground(sf::Texture* t){
    background_texture=t;
    back.setTexture(*background_texture);
    back.setScale(1, 1.2);
}


void  Wave::setSoundBuffer(sf::SoundBuffer* b){
    buff=b;
}

void  Wave::setBoomBuffer(sf::SoundBuffer* b){
    boombuff=b;
}


void Wave::killPlayer(std::vector<Player>& players, int i){
    if(players[i].immunity)
        return;
    
    boom.setBuffer(*boombuff);
    boom.play();
    boom.setVolume(1000);
    
    players[i].decrementLives();
    
    score-=20;
    

    if(players[i].getLives() > 0)
        players[i].setPosition(players[i].getIntialPosition());
    else{
        players.erase(players.begin() + i);
        i--;
    }
    
    
    death_immunity[i].restart();
}


void Wave::Animation(std::vector<Player>& players){
    
    sf::IntRect player_size(0,0,640,640);
    /*
    
    //std::cout <<"da5al" <<std::endl;
    
    if (player_clock.getElapsedTime().asSeconds() > 0.2f){
        bool minus=false;
         std::cout <<"da5al clock" <<std::endl;
       // if (player_size.left == 2560) minus=true;
        //if (player_size.left ==0) minus=false;
        
        if(minus==true) player_size.left -=640;
        else player_size.left +=640;
        
        std::cout<<player_size.left<<std::endl;
        
        
        for(int i=0; i<players.size() && i<2; i++) {
            players[i].setTextureRect(player_size);
        }
        std::cout <<"update" <<std::endl;

               // expsize.left +=266;
        // if(expsize.left==4522) sp1.exp.setPosition(372873, 372873);
        // sp1.exp.setTextureRect(expsize);
        
        player_clock.restart();
        
    }
     */
    
}

void Wave::loadPickupsTextures(sf::Texture* p, sf::Texture* w1, sf::Texture* w2, sf::Texture* w3){
    
    powerup=p;
    wep1=w1;
    wep2=w2;
    wep3=w3;
    
}

void Wave::loadWeaponsSounds(sf::SoundBuffer* bew, sf::SoundBuffer* wo, sf::SoundBuffer* bo){
    
}

void Wave::changeWeapon(Player& p, int t){
    switch(t){
        case 6: {
            new_b.setTexture(fork);
            p.setBulletTexture(fork);
            
        } break;
        case 7: {
            new_b.setTexture(fire);
            p.setBulletTexture(fire);

        } break;
        case 8: {
            new_b.setTexture(beam);
            p.setBulletTexture(beam);

        } break;
    }
    
}
void Wave::incBullSize(Player& p){
    p.setBulletScale(p.GetBulletScale().x+0.2,p.GetBulletScale().y+0.2);

    
}


void Wave::setscoretext()
{
    scoretext.setString(std::to_string(score));
    scoretext.setCharacterSize(100);
    //scoretext.setOrigin(0, 0);
    scoretext.setPosition(0, 0);
    scoretext.setFillColor(sf::Color::White);
    scoretext.setFont(*font);
}


void Wave::setFontTexture(sf::Font* f){
    font=f;
    
}


