//
//  Small_Moving_Objects.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Small_Moving_Objects.hpp"


void Small_Moving_Objects::setStrength(int s) {
    
    strength=s;
}

int Small_Moving_Objects::getStrength() {
    return strength;
}
