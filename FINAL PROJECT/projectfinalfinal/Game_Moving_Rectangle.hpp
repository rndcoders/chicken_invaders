//
//  Game_Moving_Rectangle.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//





#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include "Game_Moving_Object.hpp"
#ifndef Game_Moving_Rectangle_hpp
#define Game_Moving_Rectangle_hpp

class Game_Moving_Rectangle : public Game_Moving_Object{
    
    
protected:
    sf::RectangleShape R;
public:
    virtual ~Game_Moving_Rectangle();
    
    
    void setTexture(sf::Texture* t);
    void setTextureRect(const sf::IntRect& I) ;
    
    void setPosition(const sf::Vector2f& P) ;
   const sf::Vector2f& getPosition() const ;
    
    void move(const sf::Vector2f& D) ;
   void move(float x, float y);
    void move() ;

    
    void draw(sf::RenderWindow& window) const ;
    
  void setColor(const sf::Color& c) ;
   const sf::Color& getColor() const ;

    
    void setScale(float x, float y);
    void setScale(const sf::Vector2f& s);
    
    const sf::Vector2f& getScale() const;
    
    const sf::Vector2f& getSize() const;
    
    sf::FloatRect getGlobalBounds() const;

};


#endif /* Game_Moving_Rectangle_hpp */
