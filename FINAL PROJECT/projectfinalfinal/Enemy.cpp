//
//  Enemy.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Enemy.hpp"

void Enemy::setStrength(int s){
    strength=s;
}
int Enemy::getStrength(){
    return strength;
}
void Enemy::decStrength(int s){
    strength-=s;
    
}
void Enemy::incStrength(int s){
    strength+=s;
    
}
