//
//  SetStartMenu.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/6/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef SetStartMenu_hpp
#define SetStartMenu_hpp

#include <stdio.h>
#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include<cstdlib>
#include<string>
#include <random>
#include <time.h>
#include <string>
#include "ResourcePath.hpp"

using namespace sf;
//using namespace std;

class SetStartMenu
{
public:
    void initialize(Color, Color, Color, Color);
    void render(RenderWindow &Window);
    void handle(RenderWindow &Window, Vector2i &location, int &tap, Color, Color, Color, Color, bool& flag);
    //void CheckMouse(int &current, int &previous, Vector2i location, RenderWindow &Window);
    //bool CheckFlag();
    
    
private:
    RectangleShape NewGameRect, LoadGameRect, SettingsRect, ExitRect, mouser;
    Text textStartGame, textLoadGame, textSettings, textExit, textHeadline, Credits;
    Font font, font2;
    Texture texture, texture2;
    Sprite sprite;
    Music music;
    Sound sound;
    
};

#endif /* SetStartMenu_hpp */
