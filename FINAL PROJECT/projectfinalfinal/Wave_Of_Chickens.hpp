//
//  Wave_Of_Chickens.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Wave_Of_Chickens_hpp
#define Wave_Of_Chickens_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include "Player.hpp"
#include "Wave.hpp"
#include "Chicken.hpp"
#include "Egg.hpp"
#include "ResourcePath.hpp"
#include "Pickups.hpp"


class Wave_Of_Chickens: public Wave{
private:
    std::vector < std::vector<Chicken> > chickens;
    std::vector < std::vector < std::vector <Egg> > > eggs;
    void drawWave(sf::RenderWindow& window, std::vector<Player>& players);
    sf::Texture* chicken_texture;
    sf::Texture* egg_texture;
    
    void createChickens();
    sf::IntRect chicken_size;
    int rows;
    int cols;
    sf::Vector2f chick_base_position;
    int probability;    
   
    
    sf::Font* font;
    sf::Text text;
    
    sf::Music music;
    
    sf::SoundBuffer* chick_hit;
    sf::Sound hit;
    
    sf::SoundBuffer* egg_lay;
    sf::Sound lay;
    
    
    sf::Vector2f chick_moving_pattern;
    
    
    

    
public:
    
    int total_chickens=rows*cols;
    int shot_chickens=0;
    
    Wave_Of_Chickens();
    Wave_Of_Chickens(std::vector<Player>& p, sf::Texture* chicken_t, sf::Texture* egg_t);
    void setChickenTexture(sf::Texture* t);
    void setEggTexture(sf::Texture* t);
     ~Wave_Of_Chickens();
     bool playWave(sf::RenderWindow& window, std::vector<Player>& players, int& new_score);
     bool win();
     int getNumEnemies();
    void moveEggs();
    void setBasePosition(sf::Vector2f& b);

    int getProbability();
    void setEggProbability(int p);
    void setPickupProbability(int p);
    
    
    void dropEggs();
    void destroyEggs(sf::RenderWindow& window);
    void createEgg(Egg& egg, Chicken& chick);
    
    void createChicken(Chicken& chick);
    
    void flipChickensRow(std::vector<Chicken>& chick);
    
    void moveChickens(sf::RenderWindow& window);

    void collisionChickBullet();
    void collisionPlayerEgg(std::vector<Player>& players);
    void collisionChickPlayer(std::vector<Player>& players);
    
    
    const sf::Vector2f& getBasePosition();
    
    void setRows(int r);
    void setCols(int c);
    
    bool winWave();
    
    void setWinText();
    
    void setMusic();
    
   
    void setChickBuffer(sf::SoundBuffer* b);
    
    void setEggBuffer(sf::SoundBuffer* e);
    
    
    void setChickMovingPattern(sf::Vector2f m);
    const sf::Vector2f& getChickMovingPattern();
    
    
    
    

    
};

#endif /* Wave_Of_Chickens_hpp */
