//
//  SetSettingsMenu.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/6/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef SetSettingsMenu_hpp
#define SetSettingsMenu_hpp

#include <stdio.h>
#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <cstdlib>
#include <string>
#include <random>
#include <time.h>
#include "ResourcePath.hpp"


using namespace sf;
using namespace std;


class SetSettingsMenu {
    
public:
    
    void initialize(Color, Color, Color, Color);
    void render(RenderWindow &Window);
    void handle(RenderWindow &Window, Vector2i &location, int &tap, Color, Color, Color, Color, bool &flag, Event event, Music &music);
    
    
private:
    int CellSize;
    RectangleShape SoundRect, SoundRectCheck, MusicRect, MusicRectCheck, BackRect;
    Text SoundText, MusicText, BackText, textHeadline, SettingsText;
    Font font, font2;
    Texture texture;
    Sprite sprite;
    
    
    
};

#endif /* SetSettingsMenu_hpp */
