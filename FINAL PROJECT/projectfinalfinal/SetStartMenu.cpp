//
//  SetStartMenu.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/6/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <time.h>
#include <stdlib.h>

#include "SetStartMenu.hpp"


using namespace sf;
using namespace std;



void SetStartMenu::initialize(Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue) {
    
    if (!texture.loadFromFile(resourcePath() + "Cover.jpg")) {
        cout << "error cover photo" << endl;
    }
    sprite.setTexture(texture);
    sprite.setScale(1.5, 1.5); 
    
    
    
    
    
    
    
    //new game shape
    NewGameRect.setSize(Vector2f(600, 100));
    NewGameRect.setPosition(100, 200);
    NewGameRect.setOutlineThickness(4);
    NewGameRect.setOutlineColor(Blue2);
    
    //Settings shape
    SettingsRect.setSize(Vector2f(600, 100));
    SettingsRect.setPosition(100, 350);
    SettingsRect.setOutlineThickness(4);
    SettingsRect.setOutlineColor(Blue2);
    SettingsRect.setFillColor(LightBlue);
    
    //Exit shape
    ExitRect.setSize(Vector2f(600, 100));
    ExitRect.setPosition(100, 500);
    ExitRect.setOutlineThickness(4);
    ExitRect.setOutlineColor(Blue2);
    
    
    
    
    //text
    //if (!font.loadFromFile(resourcePath()+  "fontt.ttf")) {
       // cout << "font error " << endl;
   // }
    if (!font2.loadFromFile(resourcePath()+  "sansation.ttf")) {
        cout << "sansation font error " << endl;
    }
    
    
    //Headline text
    textHeadline.setFont(font);
    textHeadline.setString("Chicken Invaders");
    textHeadline.setCharacterSize(150);
    textHeadline.setFillColor(Blue2);
    textHeadline.move(290, 10);
    
    
    // start game text
    textStartGame.setFont(font2);
    textStartGame.setString("New Game");
    textStartGame.setCharacterSize(60);
    textStartGame.setFillColor(Blue2);
    textStartGame.move(260, 210);
    
    
    // Settings text
    textSettings.setFont(font2);
    textSettings.setString("Settings");
    textSettings.setCharacterSize(60);
    textSettings.setFillColor(Blue2);
    textSettings.move(260, 360);
    
    
    // Exit text
    textExit.setFont(font2);
    textExit.setString("Exit");
    textExit.setCharacterSize(60);
    textExit.setFillColor(Blue2);
    textExit.move(285, 510);
    
    
    
    //Credits
    
    Credits.setFont(font2);
    Credits.setString("Designed By /n rania");
    Credits.setCharacterSize(40);
    Credits.setFillColor(Blue2);
    Credits.move(340, 660);
    
    
}


void SetStartMenu::render(RenderWindow &Window) {
    
    Window.draw(sprite);
    Window.draw(NewGameRect);
    Window.draw(SettingsRect);
    Window.draw(ExitRect);
    Window.draw(textHeadline);
    Window.draw(textStartGame);
    Window.draw(textSettings);
    Window.draw(textExit);
    
    //Window.draw(Credits);

    
}




void SetStartMenu::handle(RenderWindow &Window, Vector2i &location, int &tap, Color Blue2, Color LightBlue, Color DarkBlue, Color SeaBlue, bool &flag) {
    location = Mouse::getPosition(Window);
    
    
    
    
    
    if (NewGameRect.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
    {
        tap = 2;
        flag = true;
        
    }
    
    else if ((NewGameRect.getGlobalBounds().contains(location.x, location.y) && NewGameRect.getFillColor() != Color::Red))
    {
        NewGameRect.setFillColor(SeaBlue);
    }
    else if (NewGameRect.getFillColor() != Color::Red)
    {
        NewGameRect.setFillColor(LightBlue);
    }
    
    
    
    
    if (SettingsRect.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
    {
        tap = 4;
        
    }
    else if ((SettingsRect.getGlobalBounds().contains(location.x, location.y) && SettingsRect.getFillColor() != Color::Red))
    {
        SettingsRect.setFillColor(SeaBlue);
        
    }
    else if (SettingsRect.getFillColor() != Color::Red)
    {
        SettingsRect.setFillColor(LightBlue);
    }
    
    
    
    
    
    if (ExitRect.getGlobalBounds().contains(location.x, location.y) && Mouse::isButtonPressed(Mouse::Left))
    {
        tap = 5;
        
        
    }
    else if ((ExitRect.getGlobalBounds().contains(location.x, location.y) && ExitRect.getFillColor() != Color::Red))
    {
        ExitRect.setFillColor(SeaBlue);
        
    }
    else if (ExitRect.getFillColor() != Color::Red)
    {
        ExitRect.setFillColor(LightBlue);
    }


    
    
}
