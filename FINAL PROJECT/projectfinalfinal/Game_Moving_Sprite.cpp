//
//  Game_Moving_Sprite.cpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#include "Game_Moving_Sprite.hpp"
#include <iostream>

void Game_Moving_Sprite::setTexture(sf::Texture* t){
    S.setTexture(*t);
}

void Game_Moving_Sprite::setTextureRect(const sf::IntRect& I){
    S.setTextureRect(I);
}

void Game_Moving_Sprite::setPosition(const sf::Vector2f& P)
{
    S.setPosition(P);
}
const sf::Vector2f& Game_Moving_Sprite::getPosition() const
{
    return S.getPosition();
}

void Game_Moving_Sprite::move(const sf::Vector2f& D)
{
    S.move(D);
}

void Game_Moving_Sprite::move(float x, float y)
{
    S.move(x,y);
}

void Game_Moving_Sprite::move()
{
    S.move(moving_pattern);
}

sf::Vector2f Game_Moving_Sprite::getSize() const
{
    sf::Vector2f size;
    sf::FloatRect X = S.getGlobalBounds();
    size.x = X.width;
    size.y = X.height;
    return size;
}

void Game_Moving_Sprite::draw(sf::RenderWindow& window) const
{
    window.draw(S);
}



void Game_Moving_Sprite::setColor(const sf::Color& c)
{
    S.setColor(c);
}
const sf::Color& Game_Moving_Sprite::getColor() const
{
    return S.getColor();
}

void Game_Moving_Sprite::setScale(float x, float y)
{
    S.setScale(x, y);
    
}
void Game_Moving_Sprite::setScale(const sf::Vector2f& s)
{
    S.setScale(s);
}

const sf::Vector2f& Game_Moving_Sprite::getScale() const
{
    return S.getScale();
}

sf::FloatRect Game_Moving_Sprite::getGlobalBounds() const
{
    return S.getGlobalBounds();
}



Game_Moving_Sprite::~Game_Moving_Sprite(){
    
}

bool isColliding(Game_Moving_Sprite& first, Game_Moving_Sprite& second){
    return(second.getGlobalBounds().intersects(first.getGlobalBounds()));
}










