//
//  Chicken.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Chicken_hpp
#define Chicken_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "Enemy.hpp"


class Chicken: public Enemy
{
private:
    
public:
    Chicken();
    ~Chicken();

};


#endif /* Chicken_hpp */
