//
//  Pickups.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/7/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Pickups_hpp
#define Pickups_hpp

#include <stdio.h>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <time.h>
#include "Small_Moving_Objects.hpp"


class Pickups: public Small_Moving_Objects{
protected:
    int type; //0 for power up


    
public:
    Pickups();
    ~Pickups();
    void setType(int t);
    int getType();


    
    
};
#endif /* Pickups_hpp */
