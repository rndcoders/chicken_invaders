//
//  Player.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp


#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include "Game_Moving_Sprite.hpp"
#include "Bullet.hpp"


class Player: public Game_Moving_Sprite {
private:
    int lives;
    sf::Vector2f initial;
    
protected:
    
    sf::Texture* bullet_texture;
    float* sx;
    float* sy;
    Bullet* pointer;
    
    
    
public:
    bool immunity;
    Player();
    ~Player();
    void setLives(int l);
    int getLives() const;
    void incrementLives();
    void decrementLives();
    
    void setBulletTexture(sf::Texture* t);
    sf::Texture* getBulletTexture();
    
    void setBulletScale(float x, float y);
    const sf::Vector2f& GetBulletScale();
    float*  GetBulletScaley();
    
    void setInitialPosition(const sf::Vector2f& pos);
    const sf::Vector2f& getIntialPosition();
};

#endif /* Player_hpp */
