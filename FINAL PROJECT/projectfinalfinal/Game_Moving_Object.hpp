//
//  Game_Moving_Object.hpp
//  projectfinalfinal
//
//  Created by Nadeen Mohamed on 5/4/17.
//  Copyright © 2017 Nadeen Mohamed. All rights reserved.
//


#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#ifndef Game_Moving_Object_hpp
#define Game_Moving_Object_hpp

class Game_Moving_Object{
protected:
    sf::Vector2f moving_pattern;

public:
    ~Game_Moving_Object();
    void setMovingPattern(const sf::Vector2f& P) ;
    void setMovingPattern(float x, float y);
    const sf::Vector2f& getMovingPattern() const;
};
#endif /* Game_Moving_Object_hpp */
